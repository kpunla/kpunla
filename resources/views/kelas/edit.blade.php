@extends('layouts.main')
@section('container')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible show fade col-lg-8" role="alert">
                            {{ session('error') }}
                            <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 col-12">
                                <form action="/manajemen_admin/kelas/{{ $kelas[0]->id }}" method="POST" id="form">
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <label for="id_thnpelajaran">Tahun Pelajaran</label>
                                        <select name="id_thnpelajaran" id="id_thnpelajaran" class="form-control @error('id_thnpelajaran') is-invalid @enderror">
                                            <option value="">Select Tahun Pelajaran</option>
                                            @foreach($thnpelajarans as $thnpelajaran)
                                                @if(old('id_thnpelajaran',$kelas[0]->id_thnpelajaran) == $thnpelajaran->id)
                                                    <option value="{{ $thnpelajaran->id }}" selected>{{ $thnpelajaran->nama_thnpelajaran }}</option>
                                                    @else
                                                    <option value="{{ $thnpelajaran->id }}">{{ $thnpelajaran->nama_thnpelajaran }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @error('id_thnpelajaran')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_kelas">Nama Kelas</label>
                                        <input type="text" id="nama_kelas" class="form-control @error('nama_kelas') is-invalid @enderror"
                                            placeholder="Nama Kelas" name="nama_kelas" autofocus value="{{ old('nama_kelas',$kelas[0]->nama_kelas) }}" autocomplete="off">
                                        @error('nama_kelas')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tingkat">Tingkat</label>
                                        <select name="tingkat" id="tingkat" class="form-control @error('tingkat') is-invalid @enderror">
                                            <option value="1" {{ $kelas[0]->tingkat == 1 ? 'selected' : '' }}>1</option>
                                            <option value="2" {{ $kelas[0]->tingkat == 2 ? 'selected' : '' }}>2</option>
                                            <option value="3" {{ $kelas[0]->tingkat == 3 ? 'selected' : '' }}>3</option>
                                        </select>
                                        @error('tingkat')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="walikelas">Wali Kelas</label>
                                        <input type="text" id="walikelas" class="form-control @error('walikelas') is-invalid @enderror"
                                            placeholder="Nama Wali Kelas" name="walikelas" autofocus value="{{ old('walikelas',$kelas[0]->walikelas) }}" autocomplete="off">
                                        @error('walikelas')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="total_siswa">Total Siswa</label>
                                        <input type="number" id="total_siswa" class="form-control @error('total_siswa') is-invalid @enderror"
                                            placeholder="Total Siswa" name="total_siswa" autofocus value="{{ old('total_siswa',$kelas[0]->total_siswa) }}" autocomplete="off">
                                        @error('total_siswa')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn icon icon-left btn-primary me-1 mb-1" id="btn-submit"><i class="fa fa-save"></i> Edit</button>
                                        <button type="reset" class="btn icon icon-left btn-secondary me-1 mb-1"><i class="fa fa-reply"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- // Basic multiple Column Form section end -->
<script>
    $('#btn-submit').on('click',function(e){
        e.preventDefault();
        var form = $(this).parents('form');

        Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Untuk edit data ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Edit!'
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        })
    });
</script>
@endsection