@extends('layouts.main')

@section('container')
<section class="section">
    <div class="card">
        <div class="card-header">
            <h3>Filtering</h3>
        </div>

        <div class="card-body">
            <form action="/sdp/pengajuan/filter" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="id_thnpelajaran" class="form-label">Tahun Pelajaran</label>
                            <select class="form-select" name="id_thnpelajaran" id="id_thnpelajaran">
                                @foreach($thnajarans as $t)
                                    <option value="{{ $t->id }}">{{ $t->nama_thnpelajaran }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label class="form-label">&nbsp;</label><br>
                            <div class="d-flex">
                                <button type="button" id="btn-filter" class="btn btn-success me-1">Filter <i class="fa fa-filter"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-4">
                    <div class="card card border-top border-4 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h5>Kelas X (Sepuluh)</h5>
                                    <div class="totalSisaMasuk d-flex justify-content-between">
                                      <span class="fs-6">Total</span>
                                      <span class="fs-6" id="nominaltotal1">Rp.0</span>
                                    </div>
                                    <div class="totalMasuk d-flex justify-content-between">
                                        <span class="fs-6">Total Masuk</span>
                                        <span class="fs-6" id="nominalmasuk1">Rp.0</span>
                                    </div>
                                    <div class="border-top border-2 border-secondary mt-1 mb-1"></div>
                                    <div class="totalSisaMasuk d-flex justify-content-between">
                                      <span class="fs-6">Sisa Tagihan</span>
                                      <span class="fs-6" id="nominalsisa1">Rp.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card card border-top border-4 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h5>Kelas XI (Sebelas)</h5>
                                    <div class="totalSisaMasuk d-flex justify-content-between">
                                      <span class="fs-6">Total</span>
                                      <span class="fs-6" id="nominaltotal2">Rp.0</span>
                                    </div>
                                    <div class="totalMasuk d-flex justify-content-between">
                                        <span class="fs-6">Total Masuk</span>
                                        <span class="fs-6" id="nominalmasuk2">Rp.0</span>
                                    </div>
                                    <div class="border-top border-2 border-secondary mt-1 mb-1"></div>
                                    <div class="totalSisaMasuk d-flex justify-content-between">
                                      <span class="fs-6">Sisa Tagihan</span>
                                      <span class="fs-6" id="nominalsisa2">Rp.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card card border-top border-4 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h5>Kelas XII (Dua Belas)</h5>
                                    <div class="totalSisaMasuk d-flex justify-content-between">
                                      <span class="fs-6">Total</span>
                                      <span class="fs-6" id="nominaltotal3">Rp.0</span>
                                    </div>
                                    <div class="totalMasuk d-flex justify-content-between">
                                        <span class="fs-6">Total Masuk</span>
                                        <span class="fs-6" id="nominalmasuk3">Rp.0</span>
                                    </div>
                                    <div class="border-top border-2 border-secondary mt-1 mb-1"></div>
                                    <div class="totalSisaMasuk d-flex justify-content-between">
                                      <span class="fs-6">Sisa Tagihan</span>
                                      <span class="fs-6" id="nominalsisa3">Rp.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="card" id="piechart">
                <div class="card-header">
                  <h5>Status Pembayaran</h5>
                </div>
                <div class="card-body">
                    <div id="chart-column"></div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card" id="piechart">
                <div class="card-header">
                  <h5>Pengajuan</h5>
                </div>
                <div class="card-body">
                    <div id="chart-column2"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    getTotal();

    $('#btn-filter').on('click',function(){
        getTotal();
    });

    function getTotal(){
        const id_thnpelajaran = $('#id_thnpelajaran').val();

        $.ajax({
            url:"{{ route('spmp.dashboard') }}",
            dataType:'json',
            type:'post',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{
                id_thnpelajaran,
            },
            beforeSend:function(){
              $('#nominaltotal1').html('<i class="fas fa-circle-notch fa-spin"></i>');
              $('#nominaltotal2').html('<i class="fas fa-circle-notch fa-spin"></i>');
              $('#nominaltotal3').html('<i class="fas fa-circle-notch fa-spin"></i>');

              $('#nominalmasuk1').html('<i class="fas fa-circle-notch fa-spin"></i>');
              $('#nominalmasuk2').html('<i class="fas fa-circle-notch fa-spin"></i>');
              $('#nominalmasuk3').html('<i class="fas fa-circle-notch fa-spin"></i>');

              $('#nominalsisa1').html('<i class="fas fa-circle-notch fa-spin"></i>');
              $('#nominalsisa2').html('<i class="fas fa-circle-notch fa-spin"></i>');
              $('#nominalsisa3').html('<i class="fas fa-circle-notch fa-spin"></i>');
            },
            success:function(data){

              $('#nominaltotal1').html('Rp.'+format_n(data.total.X.total));
              $('#nominaltotal2').html('Rp.'+format_n(data.total.XI.total));
              $('#nominaltotal3').html('Rp.'+format_n(data.total.XII.total));

              $('#nominalmasuk1').html('Rp.'+format_n(data.total.X.masuk));
              $('#nominalmasuk2').html('Rp.'+format_n(data.total.XI.masuk));
              $('#nominalmasuk3').html('Rp.'+format_n(data.total.XII.masuk));

              $('#nominalsisa1').html('Rp.'+format_n(data.total.X.sisa));
              $('#nominalsisa2').html('Rp.'+format_n(data.total.XI.sisa));
              $('#nominalsisa3').html('Rp.'+format_n(data.total.XII.sisa));


                chart.updateSeries([
                    {
                        name: 'Lunas',
                        data: [data.chart1.X.lunas,data.chart1.XI.lunas,data.chart1.XII.lunas]
                    },
                    {
                        name: 'Belum Lunas',
                        data: [data.chart1.X.belum_lunas,data.chart1.XI.belum_lunas,data.chart1.XII.belum_lunas]
                    }
                ]);

                chart2.updateSeries([
                    {
                        name: 'Belum Mengajukan',
                        data: [data.chart2.X.belum,data.chart2.XI.belum,data.chart2.XII.belum]
                    },
                    {
                        name: 'Sedang Mengajukan',
                        data: [data.chart2.X.sedang,data.chart2.XI.sedang,data.chart2.XII.sedang]
                    },
                    {
                        name: 'Di Tolak',
                        data: [data.chart2.X.tolak,data.chart2.XI.tolak,data.chart2.XII.tolak]
                    },
                    {
                        name: 'Di Setujui',
                        data: [data.chart2.X.setuju,data.chart2.XI.setuju,data.chart2.XII.setuju]
                    }
                ]);
            },
            error:function(data){
                console.error(data)
            },
        });
    }

    var options = {
        series: [{
          name: 'Lunas',
          data: [0, 0, 0]
        }, {
          name: 'Belum Lunas',
          data: [0, 0, 0]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        colors: ['#198754', '#dc3545'],
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false,
          style:{
            colors: ['#198754', '#dc3545']
          }
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['X', 'XI', 'XII'],
        },
        yaxis: {
          title: {
            text: 'Jumlah Siswa'
          }
        },
        fill: {
          opacity: 1,
          colors: ['#198754', '#dc3545']
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "" + val + ""
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart-column"), options);
        chart.render();

        var options2 = {
          series: [{
          name: 'Belum Mengajukan',
          data: [0, 0, 0]
        }, {
          name: 'Sedang Mengajukan',
          data: [0, 0, 0]
        },
        {
          name: 'Di Tolak',
          data: [0, 0, 0]
        },
        {
          name: 'Di Setujui',
          data: [0, 0, 0]
        }
    ],
          chart: {
          type: 'bar',
          height: 350
        },
        colors: ['#6c757d','#ffc107','#dc3545', '#198754'],
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false,
          style:{
            colors: ['#6c757d','#ffc107','#dc3545', '#198754']
          }
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['X', 'XI', 'XII'],
        },
        yaxis: {
          title: {
            text: 'Jumlah Siswa'
          }
        },
        fill: {
          opacity: 1,
          colors: ['#6c757d','#ffc107','#dc3545', '#198754']
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "" + val + ""
            }
          }
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#chart-column2"), options2);
        chart2.render();

        function format_n(n) {
          if (n) {
              return n.toString().replace(/./g, function(c, i, a) {
                  return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
              });
          } else {
              return 0;
          }
        }

</script>
@endsection