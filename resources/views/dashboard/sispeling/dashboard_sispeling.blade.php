@extends('layouts.main')

@section('container')
<section class="section">
    <div class="card">
        <div class="card-header">
            <h3>Filtering</h3>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="id_thnpelajaran" class="form-label">Tahun Pelajaran</label>
                        <select class="form-select" name="id_thnpelajaran" id="id_thnpelajaran">
                            @foreach($thnajarans as $t)
                                <option value="{{ $t->id }}">{{ $t->nama_thnpelajaran }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="tgl_awal" class="form-label">Tanggal Awal</label>
                        <input type="date" name="tgl_awal" id="tgl_awal" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="tgl_akhir" class="form-label">Tanggal Akhir</label>
                        <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label class="form-label">&nbsp;</label><br>
                        <div class="d-flex">
                            <button type="button" id="btn-filter" class="btn btn-success me-1">Filter <i class="fa fa-filter"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-4">
                    <div class="card card border-top border-4 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h5>Kelas X (Sepuluh)</h5>
                                    <span class="fs-5" id="nominal1">Rp.0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card card border-top border-4 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h5>Kelas XI (Sebelas)</h5>
                                    <span class="fs-5" id="nominal2">Rp.0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card card border-top border-4 border-primary">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h5>Kelas XII (Dua belas)</h5>
                                    <span class="fs-5" id="nominal3">Rp.0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        {{-- <div class="col-md-7">
            <div class="card" id="piechart">
                <div class="card-header">
                    <h4>Column Chart</h4>
                </div>
                <div class="card-body">
                    <div id="chart-column"></div>
                </div>
            </div>
        </div> --}}
        <div class="col-md-5">
            <div class="card" id="piechart">
                <div class="card-header">
                    <h4>Pie Chart</h4>
                </div>
                <div class="card-body">
                    <div id="chart-pie"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    getTotal();
    chartColumn();

    $('#btn-filter').on('click',function(){
        getTotal();
        chartColumn();
    });

    function getTotal(){
        const id_thnpelajaran = $('#id_thnpelajaran').val();
        const tgl_awal = $('#tgl_awal').val();
        const tgl_akhir = $('#tgl_akhir').val();

        $.ajax({
            url:"{{ route('sispeling.dashboard.getTotal') }}",
            dataType:'json',
            type:'post',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{
                id_thnpelajaran,
                tgl_awal,
                tgl_akhir
            },
            beforeSend:function(){
                $('#nominal1').html('<i class="fas fa-circle-notch fa-spin"></i>');
                $('#nominal2').html('<i class="fas fa-circle-notch fa-spin"></i>');
                $('#nominal3').html('<i class="fas fa-circle-notch fa-spin"></i>');
            },
            success:function(data){
                const nominal1 = data.nominal1;
                const nominal2 = data.nominal2;
                const nominal3 = data.nominal3;
                $('#nominal1').html('Rp.'+format_n(nominal1));
                $('#nominal2').html('Rp.'+format_n(nominal2));
                $('#nominal3').html('Rp.'+format_n(nominal3));
                chart1.updateSeries([parseInt(nominal1),parseInt(nominal2),parseInt(nominal3)]);
            },
            error:function(data){
                console.error(data)
            },
        });
    }

    var options1 = {
            series: [0,0,0],
            chart: {
            width: 380,
            type: 'pie',
            },
            labels: ['Kelas X', 'Kelas XI', 'Kelas XII'],
            responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                width: 200
                },
                legend: {
                position: 'bottom'
                }
            }
            }]
        };
        var chart1 = new ApexCharts(document.querySelector("#chart-pie"), options1);
        chart1.render();
    
        var options = {
        series: [{
        name: 'Kelas X',
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
        }, {
        name: 'Kelas XI',
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
        }, {
        name: 'Kelas XII',
        data: [0,0,0,0,0,0,0,0,0,0,0,0]
        }],
        chart: {
        type: 'bar',
        height: 350
        },
        plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
        },
        },
        dataLabels: {
        enabled: false
        },
        stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
        },
        xaxis: {
        categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Des'],
        },
        yaxis: {
        title: {
            text: ''
        }
        },
        fill: {
        opacity: 1
        },
        tooltip: {
        y: {
            formatter: function (val) {
            return "Rp." + format_n(val)
            }
        }
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#chart-column"), options);
        chart2.render();

    function chartColumn(){
        const id_thnpelajaran = $('#id_thnpelajaran').val();
        const tgl_awal = $('#tgl_awal').val();
        const tgl_akhir = $('#tgl_akhir').val();

        $.ajax({
            url:"{{ route('sispeling.dashboard.columnChart') }}",
            dataType:'json',
            type:'post',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{
                id_thnpelajaran,
                tgl_awal,
                tgl_akhir
            },
            success:function(data){
                chart2.updateSeries([
                    {
                        name: 'Kelas X',
                        data: [data.tingkat1.January, data.tingkat1.February, data.tingkat1.March, data.tingkat1.April, data.tingkat1.May, data.tingkat1.June, data.tingkat1.July, data.tingkat1.August, data.tingkat1.September,data.tingkat1.October,data.tingkat1.November,data.tingkat1.December]
                    },
                    {
                        name: 'Kelas XI',
                        data: [data.tingkat2.January, data.tingkat2.February, data.tingkat2.March, data.tingkat2.April, data.tingkat2.May, data.tingkat2.June, data.tingkat2.July, data.tingkat2.August, data.tingkat2.September,data.tingkat2.October,data.tingkat2.November,data.tingkat2.December]
                    },
                    {
                        name: 'Kelas XII',
                        data: [data.tingkat3.January, data.tingkat3.February, data.tingkat3.March, data.tingkat3.April, data.tingkat3.May, data.tingkat3.June, data.tingkat3.July, data.tingkat3.August, data.tingkat3.September,data.tingkat3.October,data.tingkat3.November,data.tingkat3.December]
                    }
                ]);
            },
            error:function(data){
                console.error(data)
            },
        });
    }

    function format_n(n) {
        if (n) {
            return n.toString().replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
            });
        } else {
            return 0;
        }
    }
</script>
@endsection