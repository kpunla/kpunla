@extends('layouts.main')
@section('container')

<!-- Basic Tables start -->
<section class="section">
    <div class="card">
        <div class="card-header">
            <h3>Filtering</h3>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="id_thnpelajaran" class="form-label">Tahun Pelajaran</label>
                        <select class="form-select" name="id_thnpelajaran" id="id_thnpelajaran">
                            @foreach($thnajarans as $t)
                                <option value="{{ $t->id }}">{{ $t->nama_thnpelajaran }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="tingkat" class="form-label">Angkatan</label>
                        <select class="form-select" name="tingkat" id="tingkat">
                            <option value="1">X (Sepuluh)</option>
                            <option value="2">XI (Sebelas)</option>
                            <option value="3">XII (Dua Belas)</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="kelas" class="form-label">Kelas</label>
                        <select class="form-select" name="kelas" id="kelas">
                            <option value="">ALL</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="tgl_awal" class="form-label">Tanggal Awal</label>
                        <input type="date" name="tgl_awal" id="tgl_awal" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label for="tgl_akhir" class="form-label">Tanggal Akhir</label>
                        <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="mb-3">
                        <label class="form-label">&nbsp;</label><br>
                        <div class="d-flex">
                            <button type="button" id="btn-filter" class="btn btn-success me-1">Filter <i class="fa fa-filter"></i></button>
                            <button type="button" id="btn-export" class="btn btn-info me-1">Export</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a href="/sispeling/pembayaran" class="btn icon icon-left btn-primary me-1 mb-1"><i class="fa fa-plus"></i> Tambah Sispeling</a>
        </div>

        <div class="card-body">
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal Sispeling</th>
                        <th>Tahun Angkatan</th>
                        <th>Angkatan</th>
                        <th>Kelas</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<!-- Basic Tables end -->

<script>

    $(function(){
        const id_thnpelajaran = $('#id_thnpelajaran').val();
        const tingkat = $('#tingkat').val();
        getKelas(id_thnpelajaran,tingkat);

        var table = $('#table').DataTable({
            processing:true,
            serverSide:true,
            ajax:{
                url:"{{ route('sispeling.datatable') }}",
                data:function(d){
                    d.id_thnpelajaran = $('#id_thnpelajaran').val();
                    d.tingkat = $('#tingkat').val();
                    d.kelas = $('#kelas').val();
                    d.tgl_awal = $('#tgl_awal').val();
                    d.tgl_akhir = $('#tgl_akhir').val();
                }
            },
            columns:[
                {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: 10
                },
                {data:'tgl_sispeling',name:'tgl_sispeling'},
                {data:'nama_thnpelajaran',name:'nama_thnpelajaran'},
                {data:'tingkat',name:'tingkat'},
                {data:'nama_kelas',name:'nama_kelas'},
                {data:'nominal',name:'nominal'},
            ]
        });

        $('#id_thnpelajaran').on('change',function(){
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const tingkat = $('#tingkat').val();
            getKelas(id_thnpelajaran,tingkat);
        });

        $('#tingkat').on('change',function(){
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const tingkat = $('#tingkat').val();
            getKelas(id_thnpelajaran,tingkat);
        });

        function getKelas(id_thnpelajaran,tingkat){
            $.ajax({
                url:'/sispeling/getKelas',
                type:'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    id_thnpelajaran,
                    tingkat,
                },
                dataType:'json',
                success:function(data){
                    $('#kelas').html(data.html);
                },
                error:function(data){
                    console.error(data);
                }
            });
        }

        $('#btn-filter').on('click',function(){
            $('#table').DataTable().draw(true);
        });

        $('#btn-export').on('click',function(){
            const tingkat = $('#tingkat').val();
            const kelas = $('#kelas').val();
            let tgl_awal = $('#tgl_awal').val();
            let tgl_akhir = $('#tgl_akhir').val();

            if(tgl_awal == ''){
                tgl_awal = 0;
            }

            if(tgl_akhir == ''){
                tgl_akhir = 0;
            }
            

            $.ajax({
                url: '/sispeling/cetak_pdf',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    tingkat,
                    kelas,
                    tgl_awal,
                    tgl_akhir,
                },
                xhrFields: {
                    responseType: 'blob'
                },
                beforeSend(){
                    $('#btn-export').prop('disabled',true);
                    $('#btn-export').html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>');
                },
                success:function(response){
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Sample.pdf";
                    link.click();
                },
                error: function(blob){
                    console.log(blob);
                },
                complete(){
                    $('#btn-export').prop('disabled',false);
                    $('#btn-export').html('Export');
                }
                
            });
        });
    });
</script>
@endsection