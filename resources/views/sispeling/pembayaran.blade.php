@extends('layouts.main')
@section('container')

<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card border-top border-4 border-primary">
                <div class="card-content">
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3">
                            <h5>Pilih Angkatan</h5>
                        </div>
                        <form action="" method="POST">
                            <div class="row">
                                <div class="col-2">
                                    <div class="mb-3">
                                        <label for="thnajaran" class="form-label">Tahun Ajaran</label>
                                        <input type="text" name="thnajaran" id="thnajaran" class="form-control" value="{{ $thnajaran }}" readonly>
                                        <input type="hidden" name="id_thnajaran" id="id_thnajaran" value="{{ $id_thnajaran; }}">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="mb-3">
                                        <label for="tingkat" class="form-label">Angkatan</label>
                                        <select class="form-select" id="tingkat">
                                            <option value="1">X (Sepuluh)</option>
                                            <option value="2">XI (Sebelas)</option>
                                            <option value="3">XII (Dua Belas)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label class="form-label">&nbsp;</label><br>
                                        <div class="d-flex">
                                            <button type="button" id="btn-filter" class="btn btn-success me-1">Filter <i class="fa fa-filter"></i></button>
                                            <button type="button" id="btn-export" class="btn btn-info me-1">Export</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card card border-top border-4 border-primary">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-3">
                                <h5>Pembayaran Sispeling</h5>
                            </div>
                            <form action="" method="POST" id="form-sispeling">
                                @csrf
                                <input type="hidden" name="id_sispeling" id="hidden_id_sispeling">
                                <input type="hidden" name="tingkat" id="hidden_tingkat">
                                <input type="hidden" name="id_jurusan" id="hidden_id_jurusan">
                                <table class="table" id="table-angkatan">
                                    <thead>
                                        <tr>
                                        <th>No</th>
                                        <th>Kelas</th>
                                        <th>Tanggal</th>
                                        <th>Besarnya</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-group-divider">
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-primary" id="btn-submit">Submit</button>
                                    <input type="hidden" name="action" id="action" value="SUBMIT">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</section>
<script>
    getAngkatan();

    const tingkat = $('#tingkat').val();
    const jurusan = $('#jurusan').val();

    $('#hidden_tingkat').val(tingkat);
    $('#hidden_id_jurusan').val(jurusan);

    $('#tingkat').on('change',function(){
        $('#hidden_tingkat').val($(this).val());
    });
    $('#jurusan').on('change',function(){
        $('#hidden_id_jurusan').val($(this).val());
    });

    $('#btn-filter').on('click',function(){
        getAngkatan();
    });

    function getAngkatan(){
        const id_thnajaran = $('#id_thnajaran').val();
        const tingkat = $('#tingkat').val();
        const loading = '<tr><td class="text-center" colspan="4"><i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span></td></tr>';

        $.ajax({
            url: '/sispeling/pembayaran/getAngkatan',
            method:'POST',
            data:{
                id_thnajaran,
                tingkat,
            },
            dataType:'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend(){
                $('#table-angkatan tbody').html(loading);
            },
            error(){
                console.error('error')
            },
            success(data){
                $('#hidden_id_sispeling').val(data.id_sispeling);
                $('#table-angkatan tbody').html(data.table);

                if(data.button == 'submit'){
                    $('#btn-submit').removeClass('btn-warning');
                    $('#btn-submit').addClass('btn-primary');
                    $('#btn-submit').html('Submit');
                    $('#action').val('SUBMIT');
                }else{
                    $('#btn-submit').removeClass('btn-primary');
                    $('#btn-submit').addClass('btn-warning');
                    $('#btn-submit').html('Edit');
                    $('#action').val('EDIT');
                }
                // if(!data.error){
                //     $('.loading').html('');
                //     $('.content').removeClass('d-none');

                //     $('#nis_hidden').val(data.nis);
                //     $('#table-informasiSiswa').html(data.getInformasiSiswa);
                //     $('#table-history').html(data.historyPengajuan);
                // }else{
                //     $('.loading').html(data.html);
                // }
            }

        });
    }

    function ubahKeRupiah(objek) {
        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;
    }

    $('#btn-submit').on('click',function(){
        const form = document.getElementById('form-sispeling');
        const formdata = new FormData(form);
        const action = $('#action').val();

        let act = 'Submit';
        if(action == 'EDIT'){
            act = 'Edit';
        }

        Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Untuk "+act+" data ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, '+act+'!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/sispeling/store',
                    method:'POST',
                    data:formdata,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend(){
                        $('#btn-submit').prop('disabled',true);
                        $('#btn-submit').html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>');
                    },
                    error(){
                        console.error('error')
                    },
                    success(data){
                        if(data.error){
                            for(let i = 0; i < data.nominal_error.length; i++){
                                $(`#nominal_${i}_error`).html(data.msg[i]);
                            }
                        }else{
                            Swal.fire(
                            '',
                            'Data berhasil di '+act+'.',
                            'success'
                            )

                            setTimeout(() => {
                                location.reload(); 
                            }, 500);
                        }
                        // $('#table-angkatan tbody').html(data.table);
                    },
                    complete(){
                        $('#btn-submit').prop('disabled',false);
                        $('#btn-submit').html('Submit');
                    }

                });
            }
        })
    });

    $('#btn-export').on('click',function(){
        const tingkat = $('#tingkat').val();
        const id_thnajaran = $('#id_thnajaran').val();

        $.ajax({
            url: '/sispeling/cetak_pdf',
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{
                tingkat,
                id_thnajaran,
            },
            xhrFields: {
                responseType: 'blob'
            },
            success:function(response){
                var blob = new Blob([response]);
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = "Sample.pdf";
                link.click();
            },
            error: function(blob){
                console.log(blob);
            }
            
        });
    });
</script>


@endsection