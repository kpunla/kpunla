<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    #header{
        text-align: center;
    }
</style>
<body>
    <div id="header">
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">KARTU SISPELING</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">SMA NEGERI 21 BANDUNG</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">TAHUN PELAJARAN 2022/2023</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:17px;">KELAS : X (Sepuluh)</span>
    </div>

    <div id="body" style="width:100%;margin-top:30px;">
        <table border="1" style="border-collapse: collapse;width:100%;" cellpadding="10">
            <tr>
                <th>NO</th>
                <th>KELAS</th>
                <th>TANGGAL</th>
                <th>BESARNYA Rp.</th>
                <th>TTD</th>
                <th>KET</th>
            </tr>
            @foreach($datas as $data)
            <tr>
                <td style="text-align: center;font-size:13px;">{{ $loop->iteration }}</td>
                <td style="text-align: center;font-size:13px;">{{ $data->nama_kelas }}</td>
                <td style="text-align: center;font-size:13px;">{{ $data->tgl_sispeling }}</td>
                <td style="text-align: center;font-size:13px;">Rp.{{ preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $data->nominal) }}</td>
                <td style="text-align: center;font-size:13px;"></td>
                <td style="text-align: center;font-size:13px;"></td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th colspan="2">JUMLAH</th>
                <th>Rp.{{ preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $total) }}</th>
                <th></th>
                <th></th>
            </tr>
        </table>
    </div>
</body>
</html>