@extends('layouts.main')
@section('container')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">

            <input type="hidden" name="nis" id="nis" value="{{ session()->get('username') }}">
            <div class="loading"></div>

            <div class="content">
                <div class="card card border-top border-4 border-primary">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-3">
                                <h5>Informasi Siswa</h5>
                            </div>
                            <div id="table-informasiSiswa">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Tahun Ajaran</th>
                                        <th>-</th>
                                    </tr>
                                    <tr>
                                        <th>NIS</th>
                                        <th>-</th>
                                    </tr>
                                    <tr>
                                        <th>Nama Siswa</th>
                                        <th>-</th>
                                    </tr>
                                    <tr>
                                        <th>Kelas</th>
                                        <th>-</th>
                                    </tr>
                                    <tr>
                                        <th>Jalur Masuk</th>
                                        <th>-</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-3">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>Total Yang Diajukan</h5>
                                        <span class="fs-5" id="total-tagihan">Rp.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>Sisa Tagihan</h5>
                                        <span class="fs-5" id="sisa-tagihan">Rp.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>File Pengajuan</h5>
                                        <div id="file-pengajuan">
                                            <button class="btn btn-danger" id="btn-file-pengajuan">Tidak ada file</button>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>Status Pembayaran</h5>
                                        <span class="fs-5 text-danger" id="status-pembayaran">BELUM LUNAS</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- ini buat bikin button navbar --}}
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-pengajuan-tab" data-bs-toggle="pill" data-bs-target="#pills-pengajuan" type="button" role="tab" aria-controls="pills-pengajuan" aria-selected="true">History Pengajuan</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-pembayaran-tab" data-bs-toggle="pill" data-bs-target="#pills-pembayaran" type="button" role="tab" aria-controls="pills-pembayaran" aria-selected="false">History Pembayaran</button>
                    </li>
                </ul>
                {{-- akhir ini buat bikin button navbar --}}
        
                <div class="tab-content" id="pills-tabContent">
                    {{-- ini tab 1 --}}
                    <div class="tab-pane fade show active" id="pills-pengajuan" role="tabpanel" aria-labelledby="pills-pengajuan-tab">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card border-top border-4 border-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div id="table-history"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- akhir tab 1 --}}
                    {{-- ini tab 2 --}}
                    <div class="tab-pane fade" id="pills-pembayaran" role="tabpanel" aria-labelledby="pills-pembayaran-tab">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card border-top border-4 border-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <button id="btn-export-all" class="btn btn-info mb-2" style="float: right;"><i class="fa fa-print"></i> Download Resi</button>
                                            <div id="table-transaksi-terakhir"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- akhir tab 2 --}}
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-file-pengajuan" tabindex="-1" aria-labelledby="modal-file-pengajuanLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-file-pengajuanLabel">File Pengajuan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<script>
    $(function(){
        getAll();

        function getAll(){
            const nis = $('#nis').val();
            // const id_thnpelajaran = $('#id_thnpelajaran').val();
            $.ajax({
                url: '/spmp/detail_history/getAll',
                method:'POST',
                data:{
                    nis,
                },
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend(){
                    $('.content').addClass('d-none');
                    $('.loading').html('<div class="text-center"><i class="text-primary fs-3 fas fa-circle-notch fa-spin"></i> <span class="text-primary fs-3">Loading...</span></div>');
                },
                error(){
                    console.error('error')
                },
                success(data){
                    if(!data.error){
                        $('.loading').html('');
                        $('.content').removeClass('d-none');
                        $('#table-informasiSiswa').html(data.getInformasiSiswa);
                        $('#table-transaksi-terakhir').html(data.getTransaksiTerakhir);
                        $('#table-history').html(data.historyPengajuan);

                        $('#total-tagihan').html(data.getInformasiPembayaran.total_tagihan);
                        $('#file-pengajuan').html(data.getInformasiPembayaran.file_pengajuan);
                        $('#status-pembayaran').html(data.getInformasiPembayaran.status_pembayaran);
                        
                        if(data.getInformasiPembayaran.status_pembayaran == 'LUNAS'){
                            $('#status-pembayaran').removeClass('text-danger');
                            $('#status-pembayaran').addClass('text-success');
                            
                            $('#btn-submit').attr('disabled','disabled');
                        }
                        
                        $('#modal-file-pengajuan .modal-body').html(data.getInformasiPembayaran.img_pengajuan);

                        $('#sisa-tagihan').html('Rp.'+format_n(data.getInformasiPembayaran.sisa_tagihan));

                        $('#id_siswa').val(data.id_siswa);
                        $('#id_pengajuan_spmp').val(data.id_pengajuan_spmp);
                        $('#total_tagihan').val(data.jml_diajukan);
                    }else{
                        $('.loading').html(data.html);
                    }
                }

            });
        }

        function format_n(n) {
            if (n) {
                return n.toString().replace(/./g, function(c, i, a) {
                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                });
            } else {
                return 0;
            }
        }

        $(document).on('click','#btn-file-pengajuan',function(e){
            e.preventDefault();
            var modalBayar = new bootstrap.Modal(document.getElementById('modal-file-pengajuan'), {
                            keyboard: false
                        })

            modalBayar.toggle()
        });

        $(document).on('click','.btn-file-pengajuan',function(e){
            $('#modal-file-pengajuan .modal-body #img-pengajuan').attr('src',$(this).data('src'));
            e.preventDefault();
            var modalBayarDetail = new bootstrap.Modal(document.getElementById('modal-file-pengajuan'), {
                            keyboard: false
                        })

            modalBayarDetail.toggle()
        });

        $('#btn-export-all').on('click',function(){
            const nis = $('#nis').val();

            $.ajax({
                url: '/spmp/pengajuan/cetak_all_pdf',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    nis,
                },
                xhrFields: {
                    responseType: 'blob'
                },
                success:function(response){
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Sample.pdf";
                    link.click();
                },
                error: function(blob){
                    console.log(blob);
                }
                
            });
        });

        $(document).on('click','.btn-export-detail',function(){
            const id = $(this).data('id');
            const nis = $('#nis').val();
            $.ajax({
                url: '/spmp/pengajuan/cetak_detail_pdf',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    nis,
                    id,
                },
                xhrFields: {
                    responseType: 'blob'
                },
                success:function(response){
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Sample.pdf";
                    link.click();
                },
                error: function(blob){
                    console.log(blob);
                }
                
            });
        });
    });

</script>
@endsection