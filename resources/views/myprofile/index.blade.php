@extends('layouts.main')
@section('container')

<!-- Basic Tables start -->
<section class="section">
    <div class="row">
        <div class="col-4">
            <div class="card border-top border-4 border-primary">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <form action="/changephoto" id="formfoto" method="POST" enctype="multipart/form-data">
                        <div class="d-flex flex-column">
                            <img src="{{ asset('storage/'.$myphoto) }}" id="imagePhoto" class="align-self-center" alt="" alt="foto" style="width: 150px; height: 150px;border-radius:50%;border:3px solid #ddd;">
                            <span class="align-self-center fw-bold mt-2">{{ session()->get('name') }}</span>
                            <span class="align-self-center">{{ session()->get('role') }}</span>
    
                            <input type="file" name="photo" id="inputPhoto" style="display: none;">
                            <button type="button" id="changePhoto" class="btn btn-primary">Upload Photo</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8">
            {{-- ini buat bikin button navbar --}}
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                @if(strtolower(session()->get('role')) == 'siswa')
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-statusspmp-tab" data-bs-toggle="pill" data-bs-target="#pills-statusspmp" type="button" role="tab" aria-controls="pills-statusspmp" aria-selected="true">Status SPMP</button>
                    </li>
                @endif
                <li class="nav-item" role="presentation">
                    <button class="nav-link {{ strtolower(session()->get('role')) == 'admin' ? 'active' : '' }}" id="pills-editpass-tab" data-bs-toggle="pill" data-bs-target="#pills-editpass" type="button" role="tab" aria-controls="pills-editpass" aria-selected="false">Edit Password</button>
                </li>
            </ul>
            {{-- akhir ini buat bikin button navbar --}}

            <div class="tab-content" id="pills-tabContent">
                @if(strtolower(session()->get('role')) == 'siswa')
                {{-- ini tab 1 --}}
                <div class="tab-pane fade show active" id="pills-statusspmp" role="tabpanel" aria-labelledby="pills-statusspmp-tab">
                    <div class="card border-top border-4 border-primary">
                        <div class="card-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Jumlah Diajukan</th>
                                    <th>Status Pengajuan</th>
                                    <th>Status Bayar</th>
                                    <th>Aksi</th>
                                </tr>
                                @foreach($datas as $data)
                                <tr>
                                    <th>Rp.{{ preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $data->jml_diajukan) }}</th>
                                    <?php
                                        if($data->id_status == 2){
                                            $bg = 'success';
                                        }else if($data->id_status == 1){
                                            $bg = 'warning';
                                        }else{
                                            $bg = 'danger';
                                        }
                                    ?>
                                        <th><span class="badge bg-{{ $bg }}">{{ $data->nama_status }}</span></th>
                                    <th><span class="badge bg-{{ $data->status_pembayaran == 'LUNAS' ? 'success' : 'danger' }}">{{ $data->status_pembayaran }}</span></th>
                                    <th><a href="/myprofile/history_spmp" class="btn btn-info btn-sm">Lihat History</a></th>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                {{-- ini tab 2 --}}
                <div class="tab-pane fade {{ strtolower(session()->get('role')) == 'admin' ? 'show active' : '' }}" id="pills-editpass" role="tabpanel" aria-labelledby="pills-editpass-tab">
                    <div class="card border-top border-4 border-primary">
                        <div class="card-body">
                                <div class="form-group">
                                    <label for="password">Password Lama</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-key"></i></span>
                                        <input type="password" id="old_password" class="form-control"
                                        placeholder="Password" name="old_password" value="{{ old('password') }}" autocomplete="off">
                                            <div class="invalid-feedback" id="old_password_error">
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password Baru</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-key"></i></span>
                                        <input type="password" id="password" class="form-control"
                                        placeholder="Password" name="password" value="{{ old('password') }}" autocomplete="off">
                                            <div class="invalid-feedback" id="password_error">
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password">Confirm Password</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-key"></i></span>
                                        <input type="password" id="confirm_password" class="form-control"
                                        placeholder="Confirm password" name="confirm_password" value="{{ old('confirm_password') }}" autocomplete="off">
                                            <div class="invalid-feedback" id="confirm_password_error">
                                            
                                            </div>
                                      </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn icon icon-left btn-primary me-1 mb-1" id="btn-submit"><i class="fa fa-save"></i> Submit</button>
                                    <button type="reset" class="btn icon icon-left btn-secondary me-1 mb-1"><i class="fa fa-reply"></i> Reset</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Basic Tables end -->
<script>
    $('#btn-submit').on('click',function(){
        const old_password = $('#old_password').val();
        const password = $('#password').val();
        const confirm_password = $('#confirm_password').val();

        if(password != confirm_password){
            $('#confirm_password').addClass('is-invalid');
            $('#confirm_password_error').html('password tidak sama');
            return false;
        }else{
            $('#confirm_password').removeClass('is-invalid');
            $('#confirm_password_error').html('');
        }

        if(old_password == ''){
            $('#old_password').addClass('is-invalid');
            $('#old_password_error').html('Harap diisi');
            return false;
        }else{
            $('#old_password').removeClass('is-invalid');
            $('#old_password_error').html('');
        }
        
        if(password == ''){
            $('#password').addClass('is-invalid');
            $('#password_error').html('Harap diisi');
            return false;
        }else{
            $('#password').removeClass('is-invalid');
            $('#password_error').html('');
        }

        if(confirm_password == ''){
            $('#confirm_password').addClass('is-invalid');
            $('#confirm_password_error').html('Harap diisi');
            return false;
        }else{
            $('#confirm_password').removeClass('is-invalid');
            $('#confirm_password_error').html('');
        }

        Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Untuk Mengubah Password?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya !'
        }).then((result) => {
            if (result.isConfirmed) {

            $.ajax({
                url:'/changepassword',
                method:'POST',
                data:{
                    old_password,
                    password,
                    confirm_password
                },
                dataType:'json',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend(){
                    $('#btn_submit').html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>')
                },
                complete(){
                    $('#btn-submit').html('Submit')
                },
                success(data){
                    $('#old_password').val("");
                    $('#password').val("");
                    $('#confirm_password').val("");
                    $('#old_password').focus();
                    if(data.error){
                        Swal.fire(
                            '',
                            data.msg,
                            'error'
                        )
                    }else{
                        Swal.fire(
                            '',
                            data.msg,
                            'success'
                        )
                    }
                }
            })
            }
        })
    })

    $('#changePhoto').on('click',function(){
        $('#inputPhoto').click();
    });

    $('#inputPhoto').change(function (e) {
        const type = e.target.files[0].name.split('.');
        type[1] = type[1].toLowerCase();
        if (type[1] == 'png' || type[1] == 'jpg' || type[1] == 'jpeg') {
            $('#imagePhoto').attr('src', URL.createObjectURL(e.target.files[0]));
        }else{
            Swal.fire(
                '',
                'File tidak diizinkan',
                'error'
            )

            return false;
        }

        $.ajax({ // create an AJAX call...
            url: '/changephoto',
            method: 'post',
            data: new FormData(document.getElementById('formfoto')),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json',
            cache: false,
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) { // on success..
                if (response.error == false) {
                    Swal.fire(
                        '',
                        response.msg,
                        'success'
                    );

                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.msg,
                    });
                }
            }
        });

    });
</script>
@endsection