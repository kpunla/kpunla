@extends('layouts.main')
@section('container')

<!-- Basic Tables start -->
<section class="section">
    <div class="card">
        <div class="card-header">
            <h3>Filtering</h3>
        </div>

        <div class="card-body">
            <form action="/sdp/pengajuan/filter" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="jurusan" class="form-label">Jurusan</label>
                            <select class="form-select" name="jurusan" id="jurusan">
                                @foreach($jurusans as $jurusan)
                                    @if($temp_jurusan == $jurusan->id)
                                        <option value="{{ $jurusan->id }}" selected>{{ $jurusan->jurusan }}</option>
                                    @else
                                        <option value="{{ $jurusan->id }}">{{ $jurusan->jurusan }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mb-3">
                            <label for="id_thnpelajaran" class="form-label">Tahun Ajaran</label>
                            <select class="form-select" name="id_thnpelajaran" id="id_thnpelajaran">
                                @foreach($thnpelajarans as $thnpelajaran)
                                    @if($temp_id_thnpelajaran == $thnpelajaran->id)
                                        <option value="{{ $thnpelajaran->id }}" selected>{{ $thnpelajaran->nama_thnpelajaran }}</option>
                                    @else
                                        <option value="{{ $thnpelajaran->id }}">{{ $thnpelajaran->nama_thnpelajaran }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="id_kelas" class="form-label">Kelas</label>
                            <select class="form-select" name="id_kelas" id="id_kelas">
                            </select>
                        </div>
                        <input type="hidden" name="temp_id_kelas" id="temp_id_kelas" value="{{ $temp_id_kelas }}">
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="nmjalurmasuk" class="form-label">Jalur Masuk</label>
                            <select class="form-select" name="nmjalurmasuk" id="nmjalurmasuk">
                                <option value="" {{ $temp_nmjalurmasuk == '' ? 'selected' : '' }}>ALL</option>
                                <option value="KETM" {{ $temp_nmjalurmasuk == 'KETM' ? 'selected' : '' }}>KETM</option>
                                <option value="SKTM" {{ $temp_nmjalurmasuk == 'SKTM' ? 'selected' : '' }}>SKTM</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label class="form-label">&nbsp;</label><br>
                            <button type="submit" id="btn-submit" class="btn btn-success">Filter <i class="fa fa-filter"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
        </div>

        <div class="card-body">
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible show fade col-lg-8" role="alert">
                {{ session('success') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger alert-dismissible show fade col-lg-8" role="alert">
                {{ session('error') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <table class="table" id="table1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Jalur Masuk</th>
                        <th>Status Pembayaran</th>
                        <th>Status Pengajuan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($siswas[0]->nis))
                        @foreach($siswas as $siswa)
                        <tr>
                            <td>{{ $loop->iteration; }}</td>
                            <td>{{ $siswa->nis }}</td>
                            <td>{{ $siswa->nmlengkap }}</td>
                            <td>{{ $siswa->nama_kelas }}</td>
                            <td>{{ $siswa->nmjalurmasuk }}</td>

                            @if($siswa->status_pembayaran == 'BELUM LUNAS' || $siswa->status_pembayaran == '')
                            <td><span class="badge bg-danger">BELUM LUNAS</span></td>
                            @else
                            <td><span class="badge bg-success">{{ $siswa->status_pembayaran }}</span></td>
                            @endif

                            @if($siswa->jml_diajukan != '')
                            <td><span class="badge bg-success">Sudah Diajukan</span></td>
                                @if($siswa->status_pembayaran == 'BELUM LUNAS')
                                    <td><a href="/spmp/transaksi/{{ $siswa->nis }}" class="btn icon btn-warning">Bayar <i class="fa fa-angle-double-right"></i></a></td>
                                @else
                                    <td><a href="/spmp/transaksi/{{ $siswa->nis }}" class="btn icon btn-primary">Lihat <i class="fa fa-angle-double-right"></i></a></td>
                                @endif
                            @else
                            <td><span class="badge bg-danger">Belum Mengajukan</span></td>
                            <td><a href="/spmp/pengajuan/{{ $siswa->id_siswa }}/edit" class="btn icon btn-danger">Ajukan <i class="fa fa-angle-double-right"></i></a></td>
                            @endif
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center">No data available in table</td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- Basic Tables end -->
<script>
    $('#table1').DataTable();
    
    const temp_id_kelas = $('#temp_id_kelas').val();
    getKelas('','',temp_id_kelas);

    function getKelas(id_jurusan = '', id_thnpelajaran = '',temp_id_kelas)
    {
        if(id_jurusan == '' && id_thnpelajaran == ''){
            id_jurusan = $('#jurusan').val();
            id_thnpelajaran = $('#id_thnpelajaran').val();
        }

        $.ajax({
            url: '/pengajuan/getKelas',
            method:'POST',
            data:{
                id_thnpelajaran,
                id_jurusan,
                temp_id_kelas,
            },
            dataType:'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success(data){
                if(data.html){
                    $('#id_kelas').html(data.html);
                }
            }
        });
    }

    $('#jurusan').on('change',function(){
        const id_jurusan = $(this).val();
        const id_thnpelajaran = $('#id_thnpelajaran').val();
        getKelas(id_jurusan,id_thnpelajaran)
    });

    $('#id_thnpelajaran').on('change',function(){
        const id_jurusan = $('#jurusan').val();
        const id_thnpelajaran = $(this).val();
        getKelas(id_jurusan,id_thnpelajaran)
    });

</script>
@endsection