@extends('layouts.main')
@section('container')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card card border-top border-4 border-primary">
                <div class="card-content">
                    <div class="card-body">
                        @if(strtolower(session()->get('role')) == 'admin' || strtolower(session()->get('role')) == 'petugas')
                        <div class="d-flex justify-content-between mb-3">
                            <h5>Filter Pengajuan SPMP</h5>
                            <a href="/spmp/manajemen" class="btn btn-primary" target="_blank"><i class="fa fa-list"></i> Referensi Data Siswa</a>
                        </div>
                        @endif

                        <form action="" method="POST" id="form">
                            @method('post')
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="NIS">NIS Siswa</label>
                                        <div class="input-group mb-3">
                                            <input type="text" name="nis" id="nis" class="form-control" placeholder="Masukan NIS Siswa" value="{{ isset($nis) ? $nis : '' }}" {{ strtolower(session()->get('role')) == 'siswa' ? 'readonly' : '' }}>
                                            <button class="btn btn-primary" type="button" id="btn-filter"><i class="fa fa-search"></i> Cari Data</button>
                                        </div>
                                        @error('nis')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 

            <div class="loading">
            </div>

            <div class="content">
                <div class="card card border-top border-4 border-primary">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-3">
                                <h5>Informasi Siswa</h5>
                            </div>
                            <div id="table-informasiSiswa">
                                    
                            </div>
                        </div>
                    </div>
                </div>
    
                {{-- ini buat bikin button navbar --}}
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-pengajuan-tab" data-bs-toggle="pill" data-bs-target="#pills-pengajuan" type="button" role="tab" aria-controls="pills-pengajuan" aria-selected="true">Form Pengajuan</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-history-tab" data-bs-toggle="pill" data-bs-target="#pills-history" type="button" role="tab" aria-controls="pills-history" aria-selected="false">History Pengajuan</button>
                    </li>
                </ul>
                {{-- akhir ini buat bikin button navbar --}}
    
                <div class="tab-content" id="pills-tabContent">
                    {{-- ini tab 1 --}}
                    <div class="tab-pane fade show active" id="pills-pengajuan" role="tabpanel" aria-labelledby="pills-pengajuan-tab">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-3">
                                        <h5>Pengajuan Sumbangan Dana Pendidikan</h5>
                                    </div>
                                    <form action="/spmp/pengajuan" id="form-spmp" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="nis" id="nis_hidden">
                                        @method('post')
                                        @csrf
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="jml_diajukan">Jumlah yang diajukan</label>
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">Rp</span>
                                                        <input type="text" name="jml_diajukan" id="jml_diajukan" class="form-control" placeholder="Masukan Nominal" value="{{ old('jml_diajukan') }}" onkeyup="ubahKeRupiah(this)">
                                                        <div class="invalid-feedback" id="jml_diajukan_error"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="NIS">File Surat Pernyataan Surat </label>
                                                    <input type="file" name="file_surat_pernyataan" id="file_surat_pernyataan" class="form-control" onchange="ValidateSingleInput(this);">
                                                    <div class="invalid-feedback" id="file_surat_pernyataan_error"></div>
                                                    <small> * File Upload yang diperbolehkan berektensi JPG,JPEG,PNG</small><br>
                                                    <small> * Maksimal Upload 2MB</small>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <button type="submit" class="btn icon icon-left btn-primary me-1 mb-1" id="btn-submit"><i class="fa fa-save"></i> Submit</button>
                                                <button type="reset" class="btn icon icon-left btn-secondary me-1 mb-1"><i class="fa fa-reply"></i> Reset</button>
                                            </div>
                                        </div>
                                        <div class="alert alert-warning mt-1" id="alertpengajuan" hidden></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- akhir tab 1 --}}
    
    
                    {{-- ini tab 2 --}}
                    <div class="tab-pane fade" id="pills-history" role="tabpanel" aria-labelledby="pills-history-tab">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card border-top border-4 border-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div id="table-history"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- akhir tab 2 --}}
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modal-file-pengajuan" tabindex="-1" aria-labelledby="modal-file-pengajuanLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal-file-pengajuanLabel">File Pengajuan</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <img src="" class="w-100 img-thumbnail" id="img-pengajuan" alt="">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

    <script>
        $('#btn-submit').on('click',function(e){
            const form = document.getElementById('form-spmp');
            const formdata = new FormData(form);

            const jml_diajukan = $('#jml_diajukan').val();
            const file_surat_pernyataan = $('#file_surat_pernyataan').val();

            if(jml_diajukan == ''){
                $('#jml_diajukan').addClass('is-invalid');
                $('#jml_diajukan_error').html('This field is required!');
                return false;
            }else{
                $('#jml_diajukan').removeClass('is-invalid');
                $('#jml_diajukan_error').html('');
            }

            if(file_surat_pernyataan == ''){
                $('#file_surat_pernyataan').addClass('is-invalid');
                $('#file_surat_pernyataan_error').html('This field is required!');
                return false;
            }else{
                $('#file_surat_pernyataan').removeClass('is-invalid');
                $('#file_surat_pernyataan_error').html('');
            }

            e.preventDefault();
            Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Untuk tambah data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Tambahkan!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // form.submit();
                    $.ajax({
                        url: '/spmp/pengajuan',
                        method:'POST',
                        data:formdata,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend(){
                            $('#btn-submit').prop('disabled',true);
                            $('#btn-submit').html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>');
                        },
                        error(){
                            console.error('error')
                        },
                        success(data){
                            if(data.error){
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'warning'
                                )
                            }else{
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'success'
                                )
                            }
                            getAll();
                            clear();
                        },
                        complete(){
                            $('#btn-submit').prop('disabled',false);
                            $('#btn-submit').html('Submit');
                        }

                    });
                }
            })
        });

        function clear(){
            $('#jml_diajukan').val('');
            $('#file_surat_pernyataan').val('');
        }

        getAll();

        $('#btn-filter').on('click',function(){
            getAll();
        });

        function getAll(){
            const nis = $('#nis').val();
            $.ajax({
                url: '/spmp/pengajuan/getAll',
                method:'POST',
                data:{
                    nis,
                },
                dataType:'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend(){
                    $('.content').addClass('d-none');
                    $('.loading').html('<div class="text-center"><i class="text-primary fs-3 fas fa-circle-notch fa-spin"></i> <span class="text-primary fs-3">Loading...</span></div>');
                },
                error(){
                    console.error('error')
                },
                success(data){
                    if(!data.error){
                        $('.loading').html('');
                        $('.content').removeClass('d-none');

                        $('#nis_hidden').val(data.nis);
                        $('#table-informasiSiswa').html(data.getInformasiSiswa);
                        $('#table-history').html(data.historyPengajuan);

                        if(data.pengajuan){
                            $('#btn-submit').prop('disabled',true);
                            $('#alertpengajuan').removeAttr('hidden');
                            if(data.pengajuan == 'Pengajuan SPMP Sudah di setujui'){
                                $('#alertpengajuan').removeClass('alert-warning');
                                $('#alertpengajuan').addClass('alert-success');
                            }else{
                                $('#alertpengajuan').removeClass('alert-success');
                                $('#alertpengajuan').addClass('alert-warning');

                                $('#btn-submit').prop('disabled',false);
                                $('#btn-submit').removeClass('bg-primary');
                                $('#btn-submit').addClass('bg-warning');
                                $('#btn-submit').html('<i class="fa fa-pencil-alt"></i> Update');
                            }
                            $('#alertpengajuan').html('<i class="fa fa-info-circle" aria-hidden="true"></i> '+data.pengajuan);
                        }else{
                            $('#btn-submit').prop('disabled',false);
                            $('#alertpengajuan').attr('hidden','hidden');
                        }

                    }else{
                        $('.loading').html(data.html);
                    }
                }

            });
        }

        $(document).on('click','.btn-file-pengajuan',function(e){
            $('#modal-file-pengajuan .modal-body #img-pengajuan').attr('src',$(this).data('src'));
            e.preventDefault();
            var modalBayar = new bootstrap.Modal(document.getElementById('modal-file-pengajuan'), {
                            keyboard: false
                        })

            modalBayar.toggle()
        });

        function format_n(n) {
            if (n) {
                return n.toString().replace(/./g, function(c, i, a) {
                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                });
            } else {
                return 0;
            }
        }

        function ubahKeRupiah(objek) {
            separator = ".";
            a = objek.value;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + separator + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            objek.value = c;
        }

        var _validFileExtensions = [".png", ".jpg", ".jpeg", ".PNG",".JPG",".JPEG"];

        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                var sFilesize = Math.round(oInput.files[0].size / 1024);
                if (sFilesize >= 2048) {
                    alert("Maaf, " + sFileName + " ukuran file tidak diijinkan, ukuran file lebih dari 2MB.");
                    oInput.value = "";
                    return false;
                }

                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                    if (!blnValid) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: "Maaf, " + sFileName + " tipe dokumen yang tidak diijinkan, tipe dokumen yang diijinkan: " + _validFileExtensions.join(", ") + ", dan ukuran file harus di bawah 2MB.",
                        });

                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
    </script>
</section>
@endsection