<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    #header{
        text-align: center;
    }
</style>
<body>
    <div id="header">
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">RESI PEMBAYARAN</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">SUMBANGAN PENINGKATAN MUTU PENDIDIKAN</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">SMA NEGERI 21 BANDUNG</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">TAHUN PELAJARAN 2022/2023</span>
    </div>

    <div id="body" style="width:100%;margin-top:30px;">
        <table border="1" style="border-collapse: collapse;width:100%;" cellpadding="10">
            <tr class="bg-primary">
                <th class="text-white">No</th>
                <th class="text-white">Nama Pembayaran</th>
                <th class="text-white">Nominal</th>
                <th class="text-white">Tanggal</th>
            </tr>
            @foreach($trans as $data)
            <tr>
                <td style="text-align: center;">{{ $loop->iteration }}</td>
                <td style="text-align: center;">{{ $data->nama_pembayaran }}</td>
                <td style="text-align: center;">Rp.{{ preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $data->bayar) }}</td>
                <td style="text-align: center;">{{ $data->tgl_bayar }}</td>
            </tr>
            @endforeach
            <tr class="bg-primary"><th class="text-white">Total</th><th colspan="3" class="text-white">Rp.{{ preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $total_bayar[0]->total_bayar) }}</th></tr>
        </table>
    </div>
</body>
</html>