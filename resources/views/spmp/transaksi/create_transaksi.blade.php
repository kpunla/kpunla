@extends('layouts.main')
@section('container')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card card border-top border-4 border-primary">
                <div class="card-content">
                    <div class="card-body">
                        @if(strtolower(session()->get('role')) == 'admin' || strtolower(session()->get('role')) == 'petugas')
                        <div class="d-flex justify-content-between mb-3">
                            <h5>Filter Transaksi Pembayaran</h5>
                            <a href="/spmp/manajemen" class="btn btn-primary" target="_blank"><i class="fa fa-list"></i> Referensi Data Siswa</a>
                        </div>
                        @endif

                        <form action="" method="POST" id="form">
                            @method('post')
                            @csrf
                            <div class="row">
                                {{-- <div class="col-6">
                                    <div class="form-group">
                                        <label for="id_thnpelajaran">Pilih Tahun Pelajaran</label>
                                        <select name="id_thnpelajaran" id="id_thnpelajaran" class="form-control @error('id_thnpelajaran') is-invalid @enderror">
                                            @foreach($thnpelajarans as $thnpelajaran)
                                            <option value="{{ $thnpelajaran->id }}">{{ $thnpelajaran->nama_thnpelajaran }}</option>
                                            @endforeach
                                        </select>
                                        @error('id_thnpelajaran')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div> --}}
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="NIS">NIS Siswa</label>
                                        <div class="input-group mb-3">
                                            <input type="text" name="nis" id="nis" class="form-control" placeholder="Masukan NIS Siswa" value="{{ isset($nis) ? $nis : '' }}" {{ strtolower(session()->get('role')) == 'siswa' ? 'readonly' : '' }}>
                                            <button class="btn btn-primary" type="button" id="btn-filter"><i class="fa fa-search"></i> Cari Data</button>
                                        </div>
                                        @error('nis')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="loading">
            </div>

            <div class="content">
                <div class="card card border-top border-4 border-primary">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-3">
                                <h5>Informasi Siswa</h5>
                            </div>
                            <div id="table-informasiSiswa">
                                
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-4">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>Total Yang Diajukan</h5>
                                        <span class="fs-5" id="total-tagihan">Rp.0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>File Pengajuan</h5>
                                        <div id="file-pengajuan">
                                            <button class="btn btn-danger" id="btn-file-pengajuan">Tidak ada file</button>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <h5>Status Pembayaran</h5>
                                        <span class="fs-5 text-danger" id="status-pembayaran">BELUM LUNAS</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                {{-- ini buat bikin button navbar --}}
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-pembayaran-tab" data-bs-toggle="pill" data-bs-target="#pills-pembayaran" type="button" role="tab" aria-controls="pills-pembayaran" aria-selected="true">Form Pembayaran</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-history-tab" data-bs-toggle="pill" data-bs-target="#pills-history" type="button" role="tab" aria-controls="pills-history" aria-selected="false">History Pembayaran</button>
                    </li>
                </ul>
                {{-- akhir ini buat bikin button navbar --}}
                

                <div class="tab-content" id="pills-tabContent">
                    {{-- ini tab 1 --}}
                    <div class="tab-pane fade show active" id="pills-pembayaran" role="tabpanel" aria-labelledby="pills-pembayaran-tab">
                        <div class="card card border-top border-4 border-primary">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="/spmp/transaksi" method="post" id="formTrans">
                                        <input type="hidden" name="id_siswa" id="id_siswa">
                                        <input type="hidden" name="id_kelas" id="id_kelas">
                                        <input type="hidden" name="id_pengajuan_spmp" id="id_pengajuan_spmp">
                                        <input type="hidden" name="total_tagihan" id="total_tagihan">
                                        @method('post')
                                        @csrf
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="sisa-tagihan">Sisa Tagihan</label>
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text bg-white">Rp</span>
                                                        <input type="text" id="sisa-tagihan" name="sisa-tagihan" readonly autocomplete="off" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="bayar">Bayar</label>
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text">Rp</span>
                                                        <input type="text" id="bayar" name="bayar" onkeyup="ubahKeRupiah(this)" autocomplete="off" class="form-control" autocomplete="off" autofocus required>
                                                        <div class="invalid-feedback" id="bayar_error">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="kembalian">Kembalian</label>
                                                    <div class="input-group mb-3">
                                                        <span class="input-group-text bg-white">Rp</span>
                                                        <input type="text" id="kembalian" name="kembalian" value="0" readonly autocomplete="off" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <button type="submit" class="btn icon icon-left btn-primary me-1 mb-1" id="btn-submit"><i class="fa fa-save"></i> Submit</button>
                                                <button type="reset" class="btn icon icon-left btn-secondary me-1 mb-1"><i class="fa fa-reply"></i> Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- akhir tab 1 --}}
                    
                    {{-- ini tab 2 --}}
                    <div class="tab-pane fade" id="pills-history" role="tabpanel" aria-labelledby="pills-history-tab">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card border-top border-4 border-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <button id="btn-export-all" class="btn btn-info mb-2" style="float: right;"><i class="fa fa-print"></i> Download Resi</button>
                                            <div id="table-transaksi-terakhir"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- akhir tab 2 --}}

                    
                </div>
            </div>
              
        </div>
    </div>
</section>

<div class="modal fade" id="modal-file-pengajuan" tabindex="-1" aria-labelledby="modal-file-pengajuanLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-file-pengajuanLabel">File Pengajuan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-nominal" tabindex="-1" aria-labelledby="modal-edit-nominal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-edit-nominal">Edit Nominal</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="/spmp/transaksi/editNominal" method="post" id="formEditNominal">
            @csrf
            <div class="modal-body">
                <input type="hidden" name="id_detail_pembayaran" id="id_detail_pembayaran">
                <input type="hidden" name="total_tagihan_val" id="total_tagihan_val">
                <input type="hidden" name="id_siswa_edit" id="id_siswa_edit">
                <div class="mb-3">
                    <label for="catatan" class="form-label">Nominal</label>
                    <div class="input-group"><span class="input-group-text" id="basic-addon1">Rp.</span>
                        <input type="text" name="editnominal" id="editnominal" onkeyup="ubahKeRupiah(this)" class="form-control">
                        <div class="invalid-feedback" id="editnominal_error"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning" id="btn-edit-nominal">Edit</button>
            </div>
        </form>
      </div>
    </div>
</div>

<!-- // Basic multiple Column Form section end -->
<script>
    getAll();

    $('#btn-filter').on('click',function(){
        getAll();
    });

    $('#btn-submit').on('click',function(e){
        e.preventDefault();

        const bayar = $('#bayar').val();
        if(bayar == ''){
            $('#bayar').addClass('is-invalid');
            $('#bayar_error').html('Harap diisi!');
            return false;
        }else{
            $('#bayar').removeClass('is-invalid');
            $('#bayar_error').html('');
        }

        Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Untuk tambah data ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Tambahkan!'
        }).then((result) => {
            if (result.isConfirmed) {
                const form = document.querySelector('#formTrans');
                const formdata = new FormData(form);

                $.ajax({
                    url: "/spmp/transaksi",
                    method:'POST',
                    data:formdata,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend(){
                        $('#btn-submit').prop('disabled',true);
                        $('#btn-submit').html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>');
                    },
                    error(){
                        console.error('error')
                    },
                    success(data){
                        if(data.error){
                            Swal.fire(
                                '',
                                data.msg,
                                'warning'
                            )
                        }else{
                            Swal.fire(
                                '',
                                data.msg,
                                'success'
                            )
                        }
                        getAll();
                        clear();
                    },
                    complete(){
                        $('#btn-submit').prop('disabled',false);
                        $('#btn-submit').html('Submit');
                    }

                });
            }
        })
    });

    $('#btn-edit-nominal').on('click',function(e){
        e.preventDefault();

        const id = $('#id_detail_pembayaran').val();
        const nominal = $('#editnominal').val();

        if(nominal == ''){
            $('#editnominal').addClass('is-invalid');
            $('#editnominal_error').html('Harap diisi!');
            return false;
        }else{
            $('#editnominal').removeClass('is-invalid');
            $('#editnominal_error').html('');
        }

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: `Untuk Ubah nominal ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya !'
            }).then((result) => {
                if (result.isConfirmed) {
                    const form = document.querySelector('#formEditNominal');
                    const formdata = new FormData(form);

                    $.ajax({
                        url: '/spmp/transaksi/editNominal',
                        method:'POST',
                        data:formdata,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        error(){
                            console.error('error')
                        },
                        success(data){
                            if(data.error){
                                if(data.nominal_err){
                                    $('#editnominal').addClass('is-invalid');
                                    $('#editnominal_error').html(data.nominal_err);
                                    return false;
                                }

                                Swal.fire(
                                    '',
                                    data.msg,
                                    'warning'
                                )
                            }else{
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'success'
                                )
                            }

                            getAll();
                        }

                    });
                }
        });
    });

    $('#btn-export-all').on('click',function(){
        const nis = $('#nis').val();

        $.ajax({
            url: '/spmp/pengajuan/cetak_all_pdf',
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{
                nis,
            },
            xhrFields: {
                responseType: 'blob'
            },
            success:function(response){
                var blob = new Blob([response]);
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = "Sample.pdf";
                link.click();
            },
            error: function(blob){
                console.log(blob);
            }
            
        });
    });

    $(document).on('click','.btn-export-detail',function(){
        const id = $(this).data('id');
        const nis = $('#nis').val();
        $.ajax({
            url: '/spmp/pengajuan/cetak_detail_pdf',
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{
                nis,
                id,
            },
            xhrFields: {
                responseType: 'blob'
            },
            success:function(response){
                var blob = new Blob([response]);
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = "Sample.pdf";
                link.click();
            },
            error: function(blob){
                console.log(blob);
            }
            
        });
    });

    $(document).on('click','.btn-delete-detail',function(){
        const id = $(this).data('id');
        const nis = $('#nis').val();
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: `Untuk delete data ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya !'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/spmp/transaksi/delete_detail',
                        type:'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{
                            nis,
                            id,
                        },
                        error(){
                            console.error('error')
                        },
                        success(data){
                            if(data.error){
                                Swal.fire(
                                    '',
                                    'Data berhasil di delete',
                                    'warning'
                                )
                            }else{
                                Swal.fire(
                                    '',
                                    'Data gagal berhasil di delete',
                                    'success'
                                )
                            }

                            setTimeout(() => {
                                location.reload(); 
                            }, 500);
                        }

                    });
                }
        });
    });

    function clear(){
        $('#bayar').val('');
    }

    function getAll(){
        const nis = $('#nis').val();
        // const id_thnpelajaran = $('#id_thnpelajaran').val();
        $.ajax({
            url: '/spmp/transaksi/getAll',
            method:'POST',
            data:{
                nis,
            },
            dataType:'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend(){
                $('.content').addClass('d-none');
                $('.loading').html('<div class="text-center"><i class="text-primary fs-3 fas fa-circle-notch fa-spin"></i> <span class="text-primary fs-3">Loading...</span></div>');
            },
            error(){
                console.error('error')
            },
            success(data){
                console.log(data)
                if(!data.error){
                    $('.loading').html('');
                    $('.content').removeClass('d-none');
                    $('#table-informasiSiswa').html(data.getInformasiSiswa);
                    $('#table-transaksi-terakhir').html(data.getTransaksiTerakhir);

                    $('#total-tagihan').html('Rp.'+data.getInformasiPembayaran.total_tagihan);
                    $('#total_tagihan_val').val(data.getInformasiPembayaran.total_tagihan);

                    $('#file-pengajuan').html(data.getInformasiPembayaran.file_pengajuan);
                    $('#status-pembayaran').html(data.getInformasiPembayaran.status_pembayaran);
                    
                    if(data.getInformasiPembayaran.status_pembayaran == 'LUNAS'){
                        $('#status-pembayaran').removeClass('text-danger');
                        $('#status-pembayaran').addClass('text-success');
                        
                        $('#btn-submit').attr('disabled','disabled');
                    }
                    
                    $('#modal-file-pengajuan .modal-body').html(data.getInformasiPembayaran.img_pengajuan);

                    $('#sisa-tagihan').val(format_n(data.getInformasiPembayaran.sisa_tagihan));

                    $('#id_siswa').val(data.id_siswa);
                    $('#id_siswa_edit').val(data.id_siswa);
                    $('#id_kelas').val(data.id_kelas);
                    $('#id_pengajuan_spmp').val(data.id_pengajuan_spmp);
                    $('#total_tagihan').val(data.jml_diajukan);
                }else{
                    $('.loading').html(data.html);
                }
            }

        });
    }

    $(document).on('click','#btn-file-pengajuan',function(e){
        e.preventDefault();
        var modalBayar = new bootstrap.Modal(document.getElementById('modal-file-pengajuan'), {
                        keyboard: false
                    })

                    modalBayar.toggle()
    });

    $(document).on('click','.btn-edit',function(e){
        const id_detail_pembayaran = $(this).data('id_detail_pembayaran');
        const nominal = $(this).data('nominal');
        $('#id_detail_pembayaran').val(id_detail_pembayaran);
        $('#editnominal').val(nominal);

        e.preventDefault();
        var modalEditNominal = new bootstrap.Modal(document.getElementById('modal-edit-nominal'), {
            keyboard: false
        })
        modalEditNominal.toggle()
    });

    $('#bayar').on('keyup',function(event){
        if(event.which >= 37 && event.which <= 40) return;

        var bayar = parseFloat($('#bayar').val().replace(/\./g, ""));
        var sisa_tagian = parseFloat($('#sisa-tagihan').val().replace(/\./g, ""));

        let hasil = 0;
        if(bayar > sisa_tagian){
            hasil =  bayar - sisa_tagian;
        }

        $('#kembalian').val(format_n(hasil))

    })

    function format_n(n) {
        if (n) {
            return n.toString().replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
            });
        } else {
            return 0;
        }
    }

    function ubahKeRupiah(objek) {
        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;
    }

</script>
@endsection