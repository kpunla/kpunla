@extends('layouts.main')
@section('container')

<!-- Basic Tables start -->
<section class="section">
    <div class="card">
        <div class="card-header">
            <h3>Filtering</h3>
        </div>

        <div class="card-body">
            <form action="/sdp/pengajuan/filter" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="id_thnpelajaran" class="form-label">Tahun Pelajaran</label>
                            <select class="form-select" name="id_thnpelajaran" id="id_thnpelajaran">
                                @foreach($thnajarans as $t)
                                    <option value="{{ $t->id }}">{{ $t->nama_thnpelajaran }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="tingkat" class="form-label">Angkatan</label>
                            <select class="form-select" name="tingkat" id="tingkat">
                                <option value="1">X (Sepuluh)</option>
                                <option value="2">XI (Sebelas)</option>
                                <option value="3">XII (Dua Belas)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="kelas" class="form-label">Kelas</label>
                            <select class="form-select" name="kelas" id="kelas">
                                <option value="">ALL</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="status_pengajuan" class="form-label">Status Pengajuan</label>
                            <select class="form-select" name="status_pengajuan" id="status_pengajuan">
                                <option value="ALL">ALL</option>
                                <option value="">Belum Mengajukan</option>
                                <option value="1">Sedang Mengajukan</option>
                                <option value="0">Di Tolak</option>
                                <option value="2">Di Setujui</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="status_pembayaran" class="form-label">Status Pembayaran</label>
                            <select class="form-select" name="status_pembayaran" id="status_pembayaran">
                                <option value="">ALL</option>
                                <option value="LUNAS">LUNAS</option>
                                <option value="BELUM LUNAS">BELUM LUNAS</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label class="form-label">&nbsp;</label><br>
                            <div class="d-flex">
                                <button type="button" id="btn-filter" class="btn btn-success me-1">Filter <i class="fa fa-filter"></i></button>
                                <button type="button" id="btn-export" class="btn btn-info me-1">Export</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
        </div>

        <div class="card-body">
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible show fade col-lg-8" role="alert">
                {{ session('success') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger alert-dismissible show fade col-lg-8" role="alert">
                {{ session('error') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Jalur Masuk</th>
                        <th>Status Pengajuan</th>
                        <th>Status Pembayaran</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-approval" tabindex="-1" aria-labelledby="modal-approvalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal-approvalLabel">Form Approval Pengajuan SPMP</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/pengajuan/storeApproval" method="post" id="formApproval">
                @csrf
                <input type="hidden" name="id" id="hidden_id">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="catatan" class="form-label">Catatan</label>
                        <textarea class="form-control" name="catatan" id="catatan" rows="3" required></textarea>
                        <div class="invalid-feedback" id="catatan_error"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="approve" value="approve" class="btn btn-success btn-act-approval">Approve</button>
                    <button type="submit" name="return" value="return" class="btn btn-danger btn-act-approval">Return</button>
                    <input type="hidden" name="action" id="action">
                </div>
            </form>
          </div>
        </div>
    </div>
</section>
<!-- Basic Tables end -->
<script>
    $(function(){
        const id_thnpelajaran = $('#id_thnpelajaran').val();
        const tingkat = $('#tingkat').val();
        getKelas(id_thnpelajaran,tingkat);
        
        $('#id_thnpelajaran').on('change',function(){
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const tingkat = $('#tingkat').val();
            getKelas(id_thnpelajaran,tingkat);
        });

        $('#tingkat').on('change',function(){
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const tingkat = $('#tingkat').val();
            getKelas(id_thnpelajaran,tingkat);
        });

        function getKelas(id_thnpelajaran,tingkat){
            $.ajax({
                url:'/sispeling/getKelas',
                type:'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    id_thnpelajaran,
                    tingkat,
                },
                dataType:'json',
                success:function(data){
                    $('#kelas').html(data.html);
                },
                error:function(data){
                    console.error(data);
                }
            });
        }

        var table = $('#table').DataTable({
            processing:true,
            serverSide:true,
            ajax:{
                url:"{{ route('spmp.manajemen.datatable') }}",
                data:function(d){
                    d.id_thnpelajaran = $('#id_thnpelajaran').val();
                    d.tingkat = $('#tingkat').val();
                    d.kelas = $('#kelas').val();
                    d.nmjalurmasuk = $('#nmjalurmasuk').val();
                    d.status_pengajuan = $('#status_pengajuan').val();
                    d.status_pembayaran = $('#status_pembayaran').val();
                }
            },
            columns:[
                {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: 10
                },
                {data:'nis',name:'nis'},
                {data:'nmlengkap',name:'nmlengkap'},
                {data:'nama_kelas',name:'nama_kelas'},
                {data:'nmjalurmasuk',name:'nmjalurmasuk'},
                {data:'status_pengajuan',name:'status_pengajuan'},
                {data:'status_pembayaran',name:'status_pembayaran'},
                {data:'action',name:'action'},
            ]
        });

        $('#btn-filter').on('click',function(){
            $('#table').DataTable().draw(true);
        });

        $('#btn-export').on('click',function(){
            const tingkat = $('#tingkat').val();
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const kelas = $('#kelas').val();
            const status_pengajuan = $('#status_pengajuan').val();
            const status_pembayaran = $('#status_pembayaran').val();

            $.ajax({
                url: '/spmp/manajemen/cetak_pdf',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    tingkat,
                    id_thnpelajaran,
                    kelas,
                    status_pengajuan,
                    status_pembayaran,
                },
                xhrFields: {
                    responseType: 'blob'
                },
                beforeSend(){
                    $('#btn-export').prop('disabled',true);
                    $('#btn-export').html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>');
                },
                success:function(response){
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Sample.pdf";
                    link.click();
                },
                error: function(blob){
                    console.log(blob);
                },
                complete(){
                    $('#btn-export').prop('disabled',false);
                    $('#btn-export').html('Export');
                }
                
            });
        });

        $(document).on('click','.btn-approval',function(e){
            e.preventDefault();
            $('#hidden_id').val($(this).data('id'));
            var modalBayar = new bootstrap.Modal(document.getElementById('modal-approval'), {
                            keyboard: false
                        })
            modalBayar.toggle()
        });

        $(document).on('click','.btn-act-approval',function(e){
            e.preventDefault();
            const catatan = document.querySelector('#catatan').value;

            if(catatan == ''){
                $('#catatan').addClass('is-invalid');
                $('#catatan_error').html('Harap diisi!');
                return false;
            }else{
                $('#catatan').removeClass('is-invalid');
                $('#catatan_error').html('');
            }

            let act = 'Approve';
            $('#action').val('approve');
            const action = e.target.value;
            if(action == 'return'){
                act = 'Return';
                $('#action').val('return')
            }

            Swal.fire({
            title: 'Apakah anda yakin?',
            text: `Untuk ${act} Pengajuan ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya !'
            }).then((result) => {
                if (result.isConfirmed) {
                    const form = document.querySelector('#formApproval');
                    const formdata = new FormData(form);

                    $.ajax({
                        url: '/pengajuan/storeApproval',
                        method:'POST',
                        data:formdata,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        error(){
                            console.error('error')
                        },
                        success(data){
                            if(data.error){
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'warning'
                                )
                            }else{
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'success'
                                )
                            }

                            setTimeout(() => {
                                location.reload(); 
                            }, 500);
                        }

                    });
                }
            });
        });
    });

</script>
@endsection