<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    #header{
        text-align: center;
    }
</style>
<body>
    <div id="header">
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">SUMBANGAN PENINGKATAN MUTU PENDIDIKAN</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">SMA NEGERI 21 BANDUNG</span>
        <span style="display: block;margin-bottom:5px;font-weight:bold;font-size:20px;">TAHUN PELAJARAN 2022/2023</span>
    </div>

    <div id="body" style="width:100%;margin-top:30px;">
        <table border="1" style="border-collapse: collapse;width:100%;" cellpadding="10">
            <tr>
                <th>NO</th>
                <th>NIS</th>
                <th>NAMA</th>
                <th>KELAS</th>
                <th>STATUS PENGAJUAN</th>
                <th>STATUS PEMBAYARAN</th>
                <th>SISA TUNGGAKAN</th>
            </tr>
            @foreach($datas as $data)
            <tr>
                <td style="text-align: center;font-size:13px;">{{ $loop->iteration }}</td>
                <td style="text-align: center;font-size:13px;">{{ $data->nis }}</td>
                <td style="text-align: center;font-size:13px;">{{ $data->nmlengkap }}</td>
                <td style="text-align: center;font-size:13px;">{{ $data->nama_kelas }}</td>
                <td style="text-align: center;font-size:13px;">{{ $data->status_pengajuan }}</td>

                @if($data->nmjalurmasuk == 'SKTM')
                    <td style="text-align: center;font-size:13px;">LUNAS</td>
                @else
                    @if($data->status_pembayaran != '')
                        <td style="text-align: center;font-size:13px;">{{ $data->status_pembayaran }}</td>
                    @else
                        <td style="text-align: center;font-size:13px;">-</td>
                    @endif
                @endif

                <?php 
                    $sisa_tagihan = ''; 
                    $query = DB::table('pembayaran_spmp')->join('pengajuan_spmp', 'pengajuan_spmp.id', '=', 'pembayaran_spmp.id_pengajuan_spmp')->where('pembayaran_spmp.id_siswa', $data->id_siswa)->get();
                ?>
                @if($data->id_status_pengajuan == 2 && isset($query[0]->total_tagihan) && $data->nmjalurmasuk != 'SKTM')
                    <?php
                    // check sisa tagihan ================================
                        $total_tagihan = $query[0]->total_tagihan;
                        $file_pengajuan = $query[0]->file_surat_pernyataan;
                        $status_pembayaran = $query[0]->status_pembayaran;

                        $bayar = DB::table('detail_pembayaran_spmp')->join('pembayaran_spmp', 'pembayaran_spmp.id', '=', 'detail_pembayaran_spmp.id_pembayaran_spmp')->select(DB::raw('SUM(bayar) as total_bayar', ''))->where('pembayaran_spmp.id_siswa', $data->id_siswa)->get();

                        if ($bayar) {
                            $sisa_tagihan = intval($query[0]->total_tagihan) - intval($bayar[0]->total_bayar);
                        } else {
                            $sisa_tagihan = intval($query[0]->total_tagihan);
                        }
                        // akhir check sisa tagihan ================================
                    ?>
                @endif;
                @if($data->nmjalurmasuk == 'SKTM')
                    <td style="text-align: center;font-size:13px;">Rp.0</td>
                @else
                    @if($sisa_tagihan != '')
                        <td style="text-align: center;font-size:13px;">Rp.{{preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $sisa_tagihan) }}</td>
                    @else
                        <td style="text-align: center;font-size:13px;">Rp.0</td>
                    @endif
                @endif
            </tr>
            @endforeach
        </table>
    </div>
</body>
</html>