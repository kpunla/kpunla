@extends('layouts.main')
@section('container')

<!-- Basic Tables start -->
<section class="section">
    <div class="card">
        <div class="card-header">
            <h3>Filtering</h3>
        </div>

        <div class="card-body">
            <form action="/sdp/pengajuan/filter" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-3">
                        <div class="mb-3">
                            <label for="id_thnpelajaran" class="form-label">Tahun Ajaran</label>
                            <select class="form-select" name="id_thnpelajaran" id="id_thnpelajaran">
                                @foreach($thnpelajarans as $thnpelajaran)
                                    <option value="{{ $thnpelajaran->id }}">{{ $thnpelajaran->nama_thnpelajaran }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="tingkat" class="form-label">Angkatan</label>
                            <select class="form-select" name="tingkat" id="tingkat">
                                <option value="1">X (Sepuluh)</option>
                                <option value="2">XI (Sebelas)</option>
                                <option value="3">XII (Dua Belas)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="id_kelas" class="form-label">Kelas</label>
                            <select class="form-select" name="id_kelas" id="id_kelas">
                                <option value="">ALL</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label for="nmjalurmasuk" class="form-label">Jalur Masuk</label>
                            <select class="form-select" name="nmjalurmasuk" id="nmjalurmasuk">
                                <option value="">ALL</option>
                                <option value="KETM">KETM</option>
                                <option value="SKTM">SKTM</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="mb-3">
                            <label class="form-label">&nbsp;</label><br>
                            <button type="button" id="btn-filter" class="btn btn-success">Filter <i class="fa fa-filter"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
        </div>

        <div class="card-body">
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible show fade col-lg-8" role="alert">
                {{ session('success') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger alert-dismissible show fade col-lg-8" role="alert">
                {{ session('error') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>
                        <th>Jalur Masuk</th>
                        <th>Status Pengajuan</th>
                        <th>File Pengajuan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-file-pengajuan" tabindex="-1" aria-labelledby="modal-file-pengajuanLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal-file-pengajuanLabel">File Pengajuan</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <img src="" class="w-100 img-thumbnail" id="img-pengajuan" alt="">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="modal-approval" tabindex="-1" aria-labelledby="modal-approvalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal-approvalLabel">Form Approval Pengajuan SPMP</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/pengajuan/storeApproval" method="post" id="formApproval">
                @csrf
                <input type="hidden" name="id" id="hidden_id">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="catatan" class="form-label">Catatan</label>
                        <textarea class="form-control" name="catatan" id="catatan" rows="3" required></textarea>
                        <div class="invalid-feedback" id="catatan_error"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="approve" value="approve" class="btn btn-success btn-act-approval">Approve</button>
                    <button type="submit" name="return" value="return" class="btn btn-danger btn-act-approval">Return</button>
                    <input type="hidden" name="action" id="action">
                </div>
            </form>
          </div>
        </div>
    </div>

</section>
<!-- Basic Tables end -->
<script>
    $(function(){
        const id_thnpelajaran = $('#id_thnpelajaran').val();
        const tingkat = $('#tingkat').val();
        getKelas(id_thnpelajaran,tingkat);

        $('#id_thnpelajaran').on('change',function(){
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const tingkat = $('#tingkat').val();
            getKelas(id_thnpelajaran,tingkat);
        });

        $('#tingkat').on('change',function(){
            const id_thnpelajaran = $('#id_thnpelajaran').val();
            const tingkat = $('#tingkat').val();
            getKelas(id_thnpelajaran,tingkat);
        });
        
        function getKelas(id_thnpelajaran,tingkat){
            $.ajax({
                url:'/sispeling/getKelas',
                type:'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    id_thnpelajaran,
                    tingkat,
                },
                dataType:'json',
                success:function(data){
                    $('#id_kelas').html(data.html);
                },
                error:function(data){
                    console.error(data);
                }
            });
        }

        $(document).on('click','.btn-file-pengajuan',function(e){
            $('#modal-file-pengajuan .modal-body #img-pengajuan').attr('src',$(this).data('src'));
            e.preventDefault();
            var modalBayar = new bootstrap.Modal(document.getElementById('modal-file-pengajuan'), {
                            keyboard: false
                        })
            modalBayar.toggle()
        });

        $(document).on('click','.btn-approval',function(e){
            e.preventDefault();
            $('#hidden_id').val($(this).data('id'));
            var modalBayar = new bootstrap.Modal(document.getElementById('modal-approval'), {
                            keyboard: false
                        })
            modalBayar.toggle()
        });

        $(document).on('click','.btn-act-approval',function(e){
            e.preventDefault();
            const catatan = document.querySelector('#catatan').value;

            if(catatan == ''){
                $('#catatan').addClass('is-invalid');
                $('#catatan_error').html('Harap diisi!');
                return false;
            }else{
                $('#catatan').removeClass('is-invalid');
                $('#catatan_error').html('');
            }

            let act = 'Approve';
            $('#action').val('approve');
            const action = e.target.value;
            if(action == 'return'){
                act = 'Return';
                $('#action').val('return')
            }

            Swal.fire({
            title: 'Apakah anda yakin?',
            text: `Untuk ${act} Pengajuan ini?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya !'
            }).then((result) => {
                if (result.isConfirmed) {
                    const form = document.querySelector('#formApproval');
                    const formdata = new FormData(form);

                    $.ajax({
                        url: '/pengajuan/storeApproval',
                        method:'POST',
                        data:formdata,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        error(){
                            console.error('error')
                        },
                        success(data){
                            if(data.error){
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'warning'
                                )
                            }else{
                                Swal.fire(
                                    '',
                                    data.msg,
                                    'success'
                                )
                            }

                            setTimeout(() => {
                                location.reload(); 
                            }, 500);
                        }

                    });
                }
            });
        });

        var table = $('#table').DataTable({
            processing:true,
            serverSide:true,
            ajax:{
                url:"{{ route('spmp.approval.datatable') }}",
                data:function(d){
                    d.id_thnpelajaran = $('#id_thnpelajaran').val();
                    d.tingkat = $('#tingkat').val();
                    d.kelas = $('#id_kelas').val();
                    d.nmjalurmasuk = $('#nmjalurmasuk').val();
                }
            },
            columns:[
                {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: 10
                },
                {data:'nis',name:'nis'},
                {data:'nmlengkap',name:'nmlengkap'},
                {data:'nama_kelas',name:'nama_kelas'},
                {data:'nmjalurmasuk',name:'nmjalurmasuk'},
                {data:'status',name:'status'},
                {data:'file_pengajuan',name:'file_pengajuan'},
                {data:'action',name:'action'},
            ]
        });

        $('#btn-filter').on('click',function(){
            $('#table').DataTable().draw(true);
        });
    });

</script>
@endsection