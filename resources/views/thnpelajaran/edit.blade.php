@extends('layouts.main')
@section('container')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible show fade col-lg-8" role="alert">
                            {{ session('error') }}
                            <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 col-12">
                                <form action="/manajemen_admin/thnpelajaran/{{ $thnpelajaran[0]->id }}" method="POST" id="form">
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_thnpelajaran">Nama Tahun Pelajaran</label>
                                        <input type="text" id="nama_thnpelajaran" class="form-control @error('nama_thnpelajaran') is-invalid @enderror"
                                            placeholder="Nama thnpelajaran" name="nama_thnpelajaran" autofocus value="{{ old('nama_thnpelajaran',$thnpelajaran[0]->nama_thnpelajaran) }}" autocomplete="off">
                                        @error('nama_thnpelajaran')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn icon icon-left btn-primary me-1 mb-1" id="btn-submit"><i class="fa fa-save"></i> Submit</button>
                                        <button type="reset" class="btn icon icon-left btn-secondary me-1 mb-1"><i class="fa fa-reply"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- // Basic multiple Column Form section end -->
<script>
    $('#btn-submit').on('click',function(e){
        e.preventDefault();
        var form = $(this).parents('form');

        Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Untuk edit data ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Edit!'
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        })
    });
</script>
@endsection