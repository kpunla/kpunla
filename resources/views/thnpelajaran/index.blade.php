@extends('layouts.main')
@section('container')

<!-- Basic Tables start -->
<section class="section">
    <div class="card">
        <div class="card-header">
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible show fade col-lg-8" role="alert">
                {{ session('success') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger alert-dismissible show fade col-lg-8" role="alert">
                {{ session('error') }}
                <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <a href="/manajemen_admin/thnpelajaran/create" class="btn icon icon-left btn-primary me-1 mb-1"><i class="fa fa-plus"></i> Tambah Tahun Pelajaran</a>
        </div>

        <div class="card-body">
            <table class="table" id="table1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tahun Pelajaran</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- Basic Tables end -->
<script>

    var table = $('#table1').DataTable({
        processing:true,
        serverSide:true,
        ajax:"{{ route('thnpelajaran.datatable') }}",
        columns:[
            {
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            width: 10
            },
            {data:'nama_thnpelajaran',name:'nama_thnpelajaran'},
            {data:'action',name:'action'},
        ]
    });

$(document).on('click','.btn-delete',function(e){
    e.preventDefault();
    var id = $(this).data('id');

    Swal.fire({
    title: 'Apakah anda yakin?',
    text: "Untuk hapus data ini?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/manajemen_admin/thnpelajaran/'+id,
                method:'DELETE',
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                dataType: 'json',
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend(){
                    $(this).prop('disabled',true);
                    $(this).html('<i class="fas fa-circle-notch fa-spin"></i> <span>Loading...</span>');
                },
                error(){
                    console.error('error')
                },
                success(data){
                    if(data.error){
                        Swal.fire(
                            '',
                            data.msg,
                            'warning'
                        )
                    }else{
                        Swal.fire(
                            '',
                            data.msg,
                            'success'
                        );
                        table.ajax.reload();

                    }
                },
                complete(){
                    $(this).prop('disabled',false);
                    $(this).html('<i class="fa fa-trash"></i>');
                }

            });
        }
    })
});
</script>
@endsection