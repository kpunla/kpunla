<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Thnpelajaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PengajuanSPMPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr = [
            'menu' => 'Sumbangan Dana Pendidikan/Pengajuan Sumbangan Dana Pendidikan',
        ];
        return view('spmp.pengajuan.create_pengajuan', $arr);
    }

    public function filter(Request $request)
    {
        if (isset($request->id_thnpelajaran)) {
            $siswa = DB::table('alokasi_kelas')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->join('siswas', 'siswas.id', '=', 'alokasi_kelas.id_siswa')->where('kelas.id_jurusan', $request->jurusan)->where('kelas.id_thnpelajaran', $request->id_thnpelajaran)->where('kelas.id', $request->id_kelas);

            if ($request->nmjalurmasuk != '') {
                $siswa = $siswa->where('siswas.nmjalurmasuk', $request->nmjalurmasuk);
            }

            $siswa = $siswa->get();
        } else {
            $siswa = DB::table('alokasi_kelas')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->join('siswas', 'siswas.id', '=', 'alokasi_kelas.id_siswa')->get();
        }

        $arr = [
            'menu' => 'Sumbangan Dana Pendidikan/Siswa',
            // 'jurusans' => Jurusan::all(),
            'thnpelajarans' => DB::table('thnpelajarans')->orderBy('id', 'desc')->get(),
            'siswas' => $siswa,
        ];

        $arr['temp_id_thnpelajaran'] = isset($request->id_thnpelajaran) ? $request->id_thnpelajaran : '';
        $arr['temp_nmjalurmasuk'] = isset($request->nmjalurmasuk) ? $request->nmjalurmasuk : '';
        $arr['temp_jurusan'] = isset($request->jurusan) ? $request->jurusan : '';
        $arr['temp_id_kelas'] = isset($request->id_kelas) ? $request->id_kelas : '';

        return view('siswa.index', $arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $siswa = DB::table('siswas')->where('nis', $request->nis)->get();

        $data['id_siswa'] = $siswa[0]->id;
        $data['jml_diajukan'] = intval(str_replace(".", "", $request->jml_diajukan));
        $data['file_surat_pernyataan'] = $request->file('file_surat_pernyataan')->store('file_surat_pernyataan');
        $data['created_by'] = $request->session()->get('id');
        $data['id_status'] = 1;

        // check bila sudah mengajukan
        $checkPengajuan = DB::table('siswas')->where('nis', $request->nis)->join('pengajuan_spmp', 'pengajuan_spmp.id_siswa', 'siswas.id')->get();
        if (isset($checkPengajuan[0]->id_siswa)) {
            $pengajuan_spmp = DB::table('pengajuan_spmp')->where('id_siswa', $checkPengajuan[0]->id_siswa)->update($data);
        } else {
            $pengajuan_spmp = DB::table('pengajuan_spmp')->insertGetId($data);
        }

        if ($pengajuan_spmp) {
            // history pengajuan======
            if (isset($checkPengajuan[0]->id_siswa)) {
                $data['action'] = 'MENGAJUKAN ULANG';
            } else {
                $data['action'] = 'MENGAJUKAN';
            }
            $this->storeHistoryPengajuan($data);
            // history pengajuan======

            echo json_encode(['error' => false, 'msg' => 'Pengajuan berhasil']);
        } else {
            echo json_encode(['error' => false, 'true' => 'Pengajuan gagal']);
        }
    }

    public function storeHistoryPengajuan($data)
    {
        DB::table('history_pengajuan_spmp')->insert($data);
    }

    public function storeHistoryPembayaran($dataPembayaran)
    {
        DB::table('history_pembayaran_spmp')->insert($dataPembayaran);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arr = [
            'menu' => 'Sumbangan Dana Pendidikan/Pengajuan Sumbangan Dana Pendidikan',
            'nis' => $id,
        ];
        return view('spmp.pengajuan.create_pengajuan', $arr);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('spmp.pengajuan.create_pengajuan', [
            'menu' => 'Sumbangan Dana Pendidikan/Pengajuan Sumbangan Dana Pendidikan',
            // 'thnpelajarans' => Thnpelajaran::where('id_jurusan')->get(),
            'siswas' => DB::table('alokasi_kelas')->join('siswas', 'siswas.id', '=', 'alokasi_kelas.id_siswa')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->join('thnpelajarans', 'thnpelajarans.id', '=', 'kelas.id_thnpelajaran')->where('siswas.id', $id)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getKelas(Request $request)
    {
        $query = Kelas::all()->where('id_jurusan', $request->id_jurusan)->where('id_thnpelajaran', $request->id_thnpelajaran);
        $html = '';

        if (count($query) > 0) {
            foreach ($query as $row) {
                $html .= '<option value="' . $row->id . '">' . $row->nama_kelas . '</option>';
            }
        } else {
            $html .= '<option value="">Kelas tidak ditemukan</option>';
        }

        echo json_encode(['html' => $html]);
    }

    public function getDataTable(Request $request)
    {
        if ($request->nmjalurmasuk == '') {
            $query = Siswa::join('alokasi_kelas', 'siswas.id', 'alokasi_kelas.id_siswa')->where('alokasi_kelas.id_kelas', $request->id_kelas)->get();
        } else {
            $query = Siswa::join('alokasi_kelas', 'siswas.id', 'alokasi_kelas.id_siswa')->where('alokasi_kelas.id_kelas', $request->id_kelas)->where('siswas.nmjalurmasuk', $request->nmjalurmasuk)->get();
        }

        $html = '';
        $no = 1;
        foreach ($query as $row) {
            $html .= '<tr>';
            $html .= '<td>' . $no++ . '</td>';
            $html .= '<td>' . $row->nis . '</td>';
            $html .= '<td>' . $row->nmlengkap . '</td>';
            $html .= '<td>XI MIPA 1</td>';
            $html .= '<td>' . $row->nmjalurmasuk . '</td>';
            $html .= '<td><span class="badge bg-danger">Belum Mengajukan</span></td>';
            $html .= '<td><a href="/spmp/pengajuan/' . $row->id . '/edit" class="btn icon btn-success">Ajukan <i class="fa fa-angle-double-right"></i></a></td>';
            $html .= '</tr>';
        }
        echo json_encode(['html' => $html]);
    }

    public function getAll(Request $request)
    {
        if ($this->getInformasiSiswa($request->nis)) {
            echo json_encode(['error' => false, 'pengajuan' => $this->getPengajuan($request->nis), 'getInformasiSiswa' => $this->getInformasiSiswa($request->nis), 'nis' => $request->nis, 'historyPengajuan' => $this->historyPengajuan($request->nis)]);
        } else {
            echo json_encode(['error' => true, 'html' => '<div class="text-center"><i class="text-primary fs-3 fa fa-exclamation-triangle"></i> <span class="text-primary fs-3"> Data Tidak Ditemukan</span></div>']);
        }
    }

    public function getPengajuan($nis)
    {
        $pengajuan = false;

        if ($nis != '') {
            $q_pengajuan = DB::select("SELECT hps.id_status FROM pengajuan_spmp hps INNER JOIN siswas s ON s.id = hps.id_siswa WHERE s.nis = $nis  ORDER BY hps.id DESC LIMIT 1");

            if (isset($q_pengajuan[0]->id_status)) {
                if ($q_pengajuan[0]->id_status == 1 || $q_pengajuan[0]->id_status == 1) {
                    $pengajuan = 'Menunggu Persetujuan dari pihak SMA Negeri 21 Bandung';
                } else {
                    if ($q_pengajuan[0]->id_status == 2) {
                        $pengajuan = 'Pengajuan SPMP Sudah di setujui';
                    }
                }
            }
        }

        return $pengajuan;
    }

    public function getInformasiSiswa($nis)
    {
        $siswa = DB::table('alokasi_kelas')->join('siswas', 'siswas.id', '=', 'alokasi_kelas.id_siswa')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->join('thnpelajarans', 'thnpelajarans.id', '=', 'kelas.id_thnpelajaran')->where('alokasi_kelas.is_active', 1)->where('siswas.nis', $nis)->get();

        $html = '<table class="table table-striped">';
        if (isset($siswa[0]->nama_thnpelajaran) && $nis != '') {
            $html .= '<tr>
                        <th>Tahun Ajaran</th>
                        <th>' . $siswa[0]->nama_thnpelajaran . '</th>
                        </tr>
                        <tr>
                            <th>NIS</th>
                            <th>' . $siswa[0]->nis . '</th>
                        </tr>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>' . $siswa[0]->nmlengkap . '</th>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <th>' . $siswa[0]->nama_kelas . '</th>
                        </tr>
                        <tr>
                            <th>Jalur Masuk</th>
                            <th>' . $siswa[0]->nmjalurmasuk . '</th>
                        </tr>';
        } else {
            $html .= '<tr>
                        <th>Tahun Ajaran</th>
                        <th>-</th>
                        </tr>
                        <tr>
                            <th>NIS</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Jalur Masuk</th>
                            <th>-</th>
                        </tr>';
            return false;
        }
        $html .= '</table>';
        return $html;
    }

    public function historyPengajuan($nis)
    {
        $query = DB::table('siswas')->where('siswas.nis', $nis)->join('history_pengajuan_spmp', 'history_pengajuan_spmp.id_siswa', '=', 'siswas.id')->join('status', 'status.id', '=', 'history_pengajuan_spmp.id_status')->select('*', 'history_pengajuan_spmp.created_date as tanggalpengajuan')->orderBy('history_pengajuan_spmp.id', 'desc')->get();

        $html = '<table class="table table-striped" id="informasiSiswa">
        <tr class="bg-primary">
            <th class="text-white">No</th>
            <th class="text-white">Tanggal</th>
            <th class="text-white">Jumlah Diajukan</th>
            <th class="text-white">File Surat Pernyataan</th>
            <th class="text-white">Status Pengajuan</th>
            <th class="text-white">Catatan</th>
            <th class="text-white">Action</th>
        </tr>';

        if (isset($query[0]->id)) {
            $no = 1;
            foreach ($query as $row) {
                $html .= '<tr>';
                $html .= '<td>' . $no++ . '</td>';
                $html .= '<td>' . $row->tanggalpengajuan . '</td>';
                $html .= '<td>Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $row->jml_diajukan) . '</td>';
                $html .= '<td><button class="btn btn-primary btn-file-pengajuan" data-src="' . asset('storage/' . $row->file_surat_pernyataan) . '">Lihat File</button></td>';

                if ($row->id_status == 0) {
                    $bg = 'danger';
                } else if ($row->id_status == 1) {
                    $bg = 'warning';
                } else {
                    $bg = 'success';
                }

                $html .= '<td><span class="badge bg-' . $bg . '">' . $row->nama_status . '</span></td>';

                if ($row->catatan != '') {
                    $html .= '<td>' . $row->catatan . '</td>';
                } else {
                    $html .= '<td>-</td>';
                }

                $html .= '<td>' . $row->action . '</td>';

                $html .= '</tr>';
            }
        } else {
            $html .= '<tr>';
            $html .= '<td colspan="7" class="text-center">No Data</td>';
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }

    public function approval()
    {
        $siswa = DB::table('siswas')->join('pengajuan_spmp', 'pengajuan_spmp.id_siswa', 'siswas.id')->join('alokasi_kelas', 'alokasi_kelas.id_siswa', 'siswas.id')->join('status', 'status.id', '=', 'pengajuan_spmp.id_status')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->select('*', 'pengajuan_spmp.id as id_pengajuan_spmp')->orderBy('pengajuan_spmp.id', 'desc')->get();

        $arr = [
            'menu' => 'Sumbangan Dana Pendidikan/Siswa',
            // 'jurusans' => Jurusan::all(),
            'thnpelajarans' => DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->orderBy('id', 'desc')->get(),
            'siswas' => $siswa,
        ];

        return view('spmp.approval.approval_pengajuan', $arr);
    }

    public function storeApproval(Request $request)
    {
        $approval = DB::table('pengajuan_spmp')->where('id', $request->id)->update(['catatan' => $request->catatan, 'id_status' => $request->action == 'approve' ? 2 : 0]);

        if ($request->action == 'approve') {
            $action = 'di Setujui';
        } else if ($request->action == 'return') {
            $action = 'di Return';
        } else {
            $action = '';
        }

        if ($action != '') {
            if ($approval) {
                // insert history ==================
                $pengajuan_spmp = DB::table('pengajuan_spmp')->where('id', $request->id)->get();
                $data = [
                    'id_siswa' => $pengajuan_spmp[0]->id_siswa,
                    'jml_diajukan' => $pengajuan_spmp[0]->jml_diajukan,
                    'file_surat_pernyataan' => $pengajuan_spmp[0]->file_surat_pernyataan,
                    'action' => $request->action == 'approve' ? 'APPROVED' : 'REJECTED',
                    'created_by' => $request->session()->get('id'),
                    'id_status' => $request->action == 'approve' ? 2 : 0,
                    'catatan' => $request->catatan,
                ];
                $this->storeHistoryPengajuan($data);
                // insert history ==================

                // jika di approve
                if ($request->action == 'approve') {
                    $dataPembayaran['id_pengajuan_spmp'] = $request->id;
                    $dataPembayaran['id_siswa'] = $pengajuan_spmp[0]->id_siswa;
                    $dataPembayaran['total_tagihan'] = $pengajuan_spmp[0]->jml_diajukan;
                    $dataPembayaran['status_pembayaran'] = 'BELUM LUNAS';
                    $dataPembayaran['created_by'] = $request->session()->get('id');
                    $dataPembayaran['created_date'] = date('Y-m-d h:i:s');

                    DB::table('pembayaran_spmp')->insert($dataPembayaran);

                    // insert history ==================
                    $dataPembayaran['action'] = $request->action == 'approve' ? 'APPROVED' : 'REJECTED';
                    $this->storeHistoryPembayaran($dataPembayaran);
                    // insert history ==================
                }

                echo json_encode(['error' => false, 'msg' => 'Pengajuan berhasil ' . $action]);
            } else {
                echo json_encode(['error' => true, 'msg' => 'Pengajuan gagal di ' . $action]);
            }
        } else {
            echo json_encode(['error' => true, 'msg' => 'Pengajuan gagal di ' . $action]);
        }
    }

    public function DTSPMPApproval(Request $request)
    {
        $where = '';
        if (isset($request->id_thnpelajaran) && $request->id_thnpelajaran != '') {
            $where .= ' WHERE k.id_thnpelajaran = ' . $request->id_thnpelajaran;
        }

        if (isset($request->tingkat) && $request->tingkat != '') {
            $where .= ' AND k.tingkat = ' . $request->tingkat;
        }

        if (isset($request->kelas) && $request->kelas != '') {
            $where .= ' AND k.id = ' . $request->kelas;
        }

        if (isset($request->nmjalurmasuk) && $request->nmjalurmasuk != '') {
            $where .= " AND s.nmjalurmasuk = '$request->nmjalurmasuk'";
        }

        $data = DB::select("SELECT *, ps.id as id_pengajuan_spmp FROM siswas s INNER JOIN pengajuan_spmp ps ON ps.id_siswa = s.id JOIN(SELECT * FROM alokasi_kelas WHERE alokasi_kelas.is_active = 1) ak ON ak.id_siswa = s.id INNER JOIN status st ON st.id = ps.id_status INNER JOIN kelas k ON k.id = ak.id_kelas $where ORDER BY ps.id DESC;");

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                if ($row->id_status == 0) {
                    $bg = 'danger';
                } else if ($row->id_status == 1) {
                    $bg = 'warning';
                } else {
                    $bg = 'success';
                }

                $btn = '<span class="badge bg-' . $bg . '">' . $row->nama_status . '</span>';
                return $btn;
            })
            ->addColumn('file_pengajuan', function ($row) {
                $btn = '<button class="btn btn-primary btn-file-pengajuan" data-src="' . asset('storage/' . $row->file_surat_pernyataan) . '">Lihat File</button>';
                return $btn;
            })
            ->addColumn('action', function ($row) {
                if ($row->id_status == 2) {
                    $btn = '<button class="btn btn-success btn-approval disabled" data-id="' . $row->id_pengajuan_spmp . '">Approve</button>';
                } else {
                    $btn = '<button class="btn btn-success btn-approval" data-id="' . $row->id_pengajuan_spmp . '">Approve</button>';
                }
                return $btn;
            })
            ->rawColumns(['action', 'status', 'file_pengajuan'])
            ->make(true);
    }
}
