<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class MyProfileController extends Controller
{
    public function index(Request $request)
    {
        $nis = $request->session()->get('username');

        $query = [];
        if (strtolower($request->session()->get('role')) == 'siswa') {
            $query = DB::select("SELECT s.nis,IFNULL(st.id,3) as id_status, IFNULL(st.nama_status,'Belum Mengajukan') as nama_status, IFNULL(ps.jml_diajukan,'-') as jml_diajukan,IFNULL(pmb.status_pembayaran,'Belum Lunas') as status_pembayaran FROM siswas s LEFT JOIN pengajuan_spmp ps ON ps.id_siswa = s.id LEFT JOIN pembayaran_spmp pmb ON pmb.id_siswa = s.id LEFT JOIN status st ON st.id = ps.id_status WHERE s.nis = $nis");
        }

        $photo = DB::table('users')->select('photo')->where('id', $request->session()->get('id'))->get();

        $myphoto = $photo[0]->photo;
        if(isset($photo[0]->photo) && $photo[0]->photo == 'myprofile.png'){
            $myphoto = 'myprofile/'.$photo[0]->photo;
        }

        return view('myprofile.index', [
            'menu' => 'My Profile',
            'datas' => $query,
            'myphoto' => $myphoto,
        ]);
    }

    public function historySpmp()
    {
        return view('myprofile.history_spmp', [
            'menu' => 'My Profile/History SPMP',
        ]);
    }

    public function changePassword(Request $request)
    {
        $users = DB::table('users')->where('id', $request->session()->get('id'))->get();

        if (Crypt::decryptString($users[0]->password) != $request->old_password) {
            echo json_encode(['error' => true, 'msg' => 'Password lama tidak sesuai']);
            exit;
        }

        $data = [
            'password' => Crypt::encryptString($request->password)
        ];

        $password = DB::table('users')->where('id', $request->session()->get('id'))->update($data);

        if ($password) {
            echo json_encode(['error' => false, 'msg' => 'Password berhasil diubah']);
        } else {
            echo json_encode(['error' => true, 'msg' => 'Password gagal diubah']);
        }
    }

    public function changePhoto(Request $request)
    {
        $data = [
            'photo' => $request->file('photo')->store('myprofile'),
            'updated_at' => date('Y-m-d h:i:s'),
        ];

        $action = DB::table('users')->where('id', $request->session()->get('id'))->update($data);

        if ($action) {
            echo json_encode(['error' => false, 'msg' => 'Photo berhasil diubah']);
        } else {
            echo json_encode(['error' => false, 'msg' => 'Photo berhasil diubah']);
        }
    }

    public function getAll(Request $request)
    {
        $nis = $request->nis;

        $siswa = DB::table('siswas')->where('siswas.nis', $nis)->where('pengajuan_spmp.id_status', 2)->join('pengajuan_spmp', 'pengajuan_spmp.id_siswa', '=', 'siswas.id')->select('siswas.*', 'pengajuan_spmp.id as id_pengajuan_spmp', 'pengajuan_spmp.jml_diajukan')->get();

        $id_siswa = isset($siswa[0]->id) ? $siswa[0]->id : '';


        if ($id_siswa != '') {
            $getInformasiSiswa = $this->getInformasiSiswa($nis);

            $getTransaksiTerakhir = $this->getTransaksiTerakhir($id_siswa);

            $getInformasiPembayaran = $this->getInformasiPembayaran($id_siswa);

            echo json_encode(['error' => false, 'getInformasiSiswa' => $getInformasiSiswa, 'id_siswa' => $id_siswa, 'jml_diajukan' => $siswa[0]->jml_diajukan, 'id_pengajuan_spmp' => $siswa[0]->id_pengajuan_spmp, 'getTransaksiTerakhir' => $getTransaksiTerakhir, 'getInformasiPembayaran' => $getInformasiPembayaran, 'historyPengajuan' => $this->historyPengajuan($request->nis)]);
        } else {
            echo json_encode(['error' => true, 'html' => '<div class="text-center"><i class="text-primary fs-3 fa fa-exclamation-triangle"></i> <span class="text-primary fs-3"> Data Tidak Ditemukan</span></div>']);
        }
    }

    public function getInformasiSiswa($nis)
    {
        $siswa = DB::table('alokasi_kelas')->join('siswas', 'siswas.id', '=', 'alokasi_kelas.id_siswa')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->join('thnpelajarans', 'thnpelajarans.id', '=', 'kelas.id_thnpelajaran')->where('siswas.nis', $nis)->get();

        $html = '<table class="table table-striped">';
        if (isset($siswa[0]->nama_thnpelajaran)) {
            $html .= '<tr>
                        <th>Tahun Ajaran</th>
                        <th>' . $siswa[0]->nama_thnpelajaran . '</th>
                        </tr>
                        <tr>
                            <th>NIS</th>
                            <th>' . $siswa[0]->nis . '</th>
                        </tr>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>' . $siswa[0]->nmlengkap . '</th>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <th>' . $siswa[0]->nama_kelas . '</th>
                        </tr>
                        <tr>
                            <th>Jalur Masuk</th>
                            <th>' . $siswa[0]->nmjalurmasuk . '</th>
                        </tr>';
        } else {
            $html .= '<tr>
                        <th>Tahun Ajaran</th>
                        <th>-</th>
                        </tr>
                        <tr>
                            <th>NIS</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Jalur Masuk</th>
                            <th>-</th>
                        </tr>';
        }
        $html .= '</table>';
        return $html;
    }

    public function getTransaksiTerakhir($id_siswa)
    {

        $html = '<table class="table table-striped">';
        $html .= '<tr class="bg-primary">
        <th class="text-white">Nama Pembayaran</th>
        <th class="text-white">Nominal</th>
        <th class="text-white">Tanggal</th>
        <th class="text-white">Action</th>
        </tr>';

        if ($id_siswa != '') {
            $trans = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('detail_pembayaran_spmp.*')->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

            $total_bayar = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select(DB::raw('SUM(detail_pembayaran_spmp.bayar) as total_bayar'))->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

            if (isset($trans[0]->id)) {
                foreach ($trans as $row) {
                    $html .= '<tr>';
                    $html .= '<th>' . $row->nama_pembayaran . '</th>';
                    $html .= '<th>Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $row->bayar) . '</th>';
                    $html .= '<th>' . $row->tgl_bayar . '</th>';
                    $html .= '<th><div class="btn-group">
                    <a href="#" class="btn btn-info btn-export-detail" data-id="' . $row->id . '" aria-current="page"><i class="fa fa-print"></i></a>
                </div></th>';
                    $html .= '</tr>';
                    $html .= '</tr>';
                }
                $html .= '<tr class="bg-primary"><th class="text-white">Total</th><th colspan="3" class="text-white">Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $total_bayar[0]->total_bayar) . '</th></tr>';
            } else {
                $html .= '<tr>
                                <th colspan="4" class="text-center">No Data</th>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                        <th>-</th>
                        <th>-</th>
                        <th>-</th>
                    </tr>';
        }

        $html .= '</table>';

        return $html;
    }

    public function getInformasiPembayaran($id_siswa)
    {

        $this->updateStatusPembayaran($id_siswa);

        $query = DB::table('pembayaran_spmp')->join('pengajuan_spmp', 'pengajuan_spmp.id', '=', 'pembayaran_spmp.id_pengajuan_spmp')->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

        $total_tagihan = $query[0]->total_tagihan;
        $file_pengajuan = $query[0]->file_surat_pernyataan;
        $status_pembayaran = $query[0]->status_pembayaran;

        $bayar = DB::table('detail_pembayaran_spmp')->join('pembayaran_spmp', 'pembayaran_spmp.id', '=', 'detail_pembayaran_spmp.id_pembayaran_spmp')->select(DB::raw('SUM(bayar) as total_bayar', ''))->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

        if ($bayar) {
            $sisa_tagihan = intval($query[0]->total_tagihan) - intval($bayar[0]->total_bayar);
        } else {
            $sisa_tagihan = intval($query[0]->total_tagihan);
        }

        return ['total_tagihan' => 'Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $total_tagihan), 'file_pengajuan' => '<button class="btn btn-primary" id="btn-file-pengajuan">Lihat File</button> ', 'status_pembayaran' => $status_pembayaran, 'sisa_tagihan' => $sisa_tagihan, 'img_pengajuan' => '<img src="' . asset('storage/' . $file_pengajuan) . '" class="w-100 img-thumbnail" id="img-pengajuan" alt="">'];
    }

    public function updateStatusPembayaran($id_siswa)
    {
        $query_pembayaran_spmp = DB::table('pembayaran_spmp')->where('id_siswa', $id_siswa)->get();
        $total_pembayaran = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('pembayaran_spmp.id', DB::raw('SUM(detail_pembayaran_spmp.bayar) as total'))->groupBy('pembayaran_spmp.id')->where('id_siswa', $id_siswa)->get();

        if (isset($total_pembayaran[0]->total)) {
            $hasil = intval($query_pembayaran_spmp[0]->total_tagihan) - intval($total_pembayaran[0]->total);
        } else {
            // jika baru mengajukan
            $hasil = 1;
        }

        if ($hasil == 0) {
            DB::table('pembayaran_spmp')->where('id_siswa', $id_siswa)->update(['status_pembayaran' => 'LUNAS']);
        } else {
            DB::table('pembayaran_spmp')->where('id_siswa', $id_siswa)->update(['status_pembayaran' => 'BELUM LUNAS']);
        }
    }

    public function historyPengajuan($nis)
    {
        $query = DB::table('siswas')->where('siswas.nis', $nis)->join('history_pengajuan_spmp', 'history_pengajuan_spmp.id_siswa', '=', 'siswas.id')->join('status', 'status.id', '=', 'history_pengajuan_spmp.id_status')->select('*', 'history_pengajuan_spmp.created_date as tanggalpengajuan')->orderBy('history_pengajuan_spmp.id', 'desc')->get();

        $html = '<table class="table table-striped" id="informasiSiswa">
        <tr class="bg-primary">
            <th class="text-white">No</th>
            <th class="text-white">Tanggal</th>
            <th class="text-white">Jumlah Diajukan</th>
            <th class="text-white">File Surat Pernyataan</th>
            <th class="text-white">Status Pengajuan</th>
            <th class="text-white">Catatan</th>
            <th class="text-white">Action</th>
        </tr>';

        if (isset($query[0]->id)) {
            $no = 1;
            foreach ($query as $row) {
                $html .= '<tr>';
                $html .= '<td>' . $no++ . '</td>';
                $html .= '<td>' . $row->tanggalpengajuan . '</td>';
                $html .= '<td>Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $row->jml_diajukan) . '</td>';
                $html .= '<td><button class="btn btn-primary btn-file-pengajuan" data-src="' . asset('storage/' . $row->file_surat_pernyataan) . '">Lihat File</button></td>';

                if ($row->id_status == 0) {
                    $bg = 'danger';
                } else if ($row->id_status == 1) {
                    $bg = 'warning';
                } else {
                    $bg = 'success';
                }

                $html .= '<td><span class="badge bg-' . $bg . '">' . $row->nama_status . '</span></td>';

                if ($row->catatan != '') {
                    $html .= '<td>' . $row->catatan . '</td>';
                } else {
                    $html .= '<td>-</td>';
                }

                $html .= '<td>' . $row->action . '</td>';

                $html .= '</tr>';
            }
        } else {
            $html .= '<tr>';
            $html .= '<td colspan="7" class="text-center">No Data</td>';
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }
}
