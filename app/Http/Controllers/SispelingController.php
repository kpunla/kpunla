<?php

namespace App\Http\Controllers;

use App\Models\Thnpelajaran;
use App\Models\Kelas;
use PDF;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class SispelingController extends Controller
{
    public function index(Request $request)
    {
        $thnajarans = DB::table('thnpelajarans')->get();
        return view('sispeling.index', [
            'menu' => 'Manajemen Sispeling',
            'thnajarans' => $thnajarans,
        ]);
    }

    public function pembayaran()
    {
        $qp = DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->get();
        // foreach ($queryThnPelajarans as $qp) {
        //     $explode = explode('/', $qp->nama_thnpelajaran);

        //     if ($explode[0] == date('Y') || $explode[1] == date('Y')) {
        //         $thnajaran = $qp->nama_thnpelajaran;
        //         $id_thnajaran = $qp->id;
        //     }
        // }

        $thnajaran = $qp[0]->nama_thnpelajaran;
        $id_thnajaran = $qp[0]->id;

        return view('sispeling.pembayaran', [
            'menu' => 'Sispeling/Pembayaran',
            'thnajaran' => $thnajaran,
            'id_thnajaran' => $id_thnajaran,
        ]);
    }

    public function edit($id_sispeling)
    {
        $queryThnPelajarans = DB::table('thnpelajarans')->get();
        $thnajaran = '';
        $id_thnajaran = '';
        foreach ($queryThnPelajarans as $qp) {
            $explode = explode('/', $qp->nama_thnpelajaran);

            if ($explode[0] == date('Y') || $explode[1] == date('Y')) {
                $thnajaran = $qp->nama_thnpelajaran;
                $id_thnajaran = $qp->id;
            }
        }

        // $jurusans = DB::table('jurusans')->get();

        return view('sispeling.pembayaran', [
            'menu' => 'Sispeling/Pembayaran',
            'thnajaran' => $thnajaran,
            'id_thnajaran' => $id_thnajaran,
            // 'jurusans' => $jurusans,
        ]);
    }

    public function getKelas(Request $request)
    {
        $query = Kelas::all()->where('id_thnpelajaran', $request->id_thnpelajaran)->where('tingkat', $request->tingkat);

        $html = '<option value="">ALL</option>';
        if (count($query) > 0) {
            foreach ($query as $row) {
                if ($request->temp_id_kelas == $row->id) {
                    $html .= '<option value="' . $row->id . '" selected>' . $row->nama_kelas . '</option>';
                } else {
                    $html .= '<option value="' . $row->id . '">' . $row->nama_kelas . '</option>';
                }
            }
        } else {
            $html = '<option value="">Kelas tidak ditemukan</option>';
        }

        echo json_encode(['html' => $html]);
    }

    public function store(Request $request)
    {
        //validation =====
        $error = true;
        $errorfield = [];
        for ($i = 0; $i < count($request->nominal); $i++) {
            $errorfield['nominal_error'][$i] = 'nominal_' . $i . '_error';
            $errorfield['msg'][$i] = '';

            if ($request->nominal[$i] == '') {
                $errorfield['error'] = true;
                $errorfield['nominal_error'][$i] = 'nominal_' . $i . '_error';
                $errorfield['msg'][$i] = 'this field is required';
                echo json_encode($errorfield);
                exit;
            }

            if (intval($request->nominal[$i]) < 0) {
                $errorfield['error'] = true;
                $errorfield['nominal_error'][$i] = 'nominal_' . $i . '_error';
                $errorfield['msg'][$i] = 'this field is required';
                echo json_encode($errorfield);
                exit;
            }
        }
        //validation =====

        $data1 = [
            'tingkat' => $request->tingkat,
            'tgl_sispeling' => date('Y-m-d'),
            'created_by' => $request->session()->get('id'),
            'created_date' => date('Y-m-d h:i:s'),
        ];

        if ($request->action == 'SUBMIT') {
            $id_sispeling = DB::table('sispeling')->insertGetId($data1);
        } else {
            DB::table('sispeling')->where('id', $request->id_sispeling)->update($data1);
            $id_sispeling = $request->id_sispeling;
        }

        for ($i = 0; $i < count($request->nominal); $i++) {
            $data['id_sispeling'] = $id_sispeling;
            $data['id_kelas'] = intval($request->id_kelas[$i]);
            $data['nominal'] = intval(str_replace(".", "", $request->nominal[$i]));
            $data['created_by'] = $request->session()->get('id');
            $data['created_date'] = date('Y-m-d h:i:s');

            if ($request->action == 'SUBMIT') {
                DB::table('detail_sispeling')->insert($data);
            } else {
                DB::table('detail_sispeling')->where('id_sispeling', $request->id_sispeling)->where('id', $request->id_detail_sispeling[$i])->update($data);
            }
        }

        echo json_encode(['error' => false]);
    }

    public function getAngkatan(Request $request)
    {
        $kelas = DB::table('kelas')->select('kelas.*', 'detail_sispeling.nominal', 'detail_sispeling.id as id_detail_sispeling', 'detail_sispeling.id_sispeling')->join('detail_sispeling', 'detail_sispeling.id_kelas', '=', 'kelas.id')->join('sispeling', 'sispeling.id', '=', 'detail_sispeling.id_sispeling')->where('kelas.id_thnpelajaran', $request->id_thnajaran)->where('sispeling.tingkat', $request->tingkat)->where('sispeling.tgl_sispeling', date('Y-m-d'))->get();

        if (!isset($kelas[0]->id_sispeling)) {
            $button = 'submit';
            $kelas = DB::table('kelas')->select('*')->where('kelas.id_thnpelajaran', $request->id_thnajaran)->where('kelas.tingkat', $request->tingkat)->get();
            $hidden_id_sispeling = '';
        } else {
            $button = 'edit';
            $hidden_id_sispeling = $kelas[0]->id_sispeling;
        }


        $table = '';
        $no = 1;
        foreach ($kelas as $key => $k) {

            $nominal = '';
            $id_detail_sispeling = '';
            if (isset($k->id_sispeling)) {
                $nominal = preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $k->nominal);
                $id_detail_sispeling = $k->id_detail_sispeling;
            }

            $table .= '<tr>';
            $table .= '<td>' . $no++ . '<input type="hidden" name="id_detail_sispeling[]" value="' . $id_detail_sispeling . '"></td>';
            $table .= '<td>' . $k->nama_kelas . '<input type="hidden" id="id_kelas" name="id_kelas[]" value="' . $k->id . '"></td>';
            $table .= '<td>' . date('Y-m-d') . '</td>';
            $table .= '<td><div class="input-group"><span class="input-group-text" id="basic-addon1">Rp.</span><input type="text" class="form-control" onkeyup="ubahKeRupiah(this)" value="' . $nominal . '" name="nominal[]" placeholder="Nominal" aria-label="Nominal" aria-describedby="basic-addon1"></div><small class="text-danger" id="nominal_' . $key . '_error"></small></td>';
            $table .= '</tr>';
        }


        echo json_encode(['table' => $table, 'button' => $button, 'id_sispeling' => $hidden_id_sispeling]);
    }

    public function DTSispeling(Request $request)
    {
        $where = '';
        if ($request->kelas != '') {
            $where .= ' AND k.id = ' . $request->kelas;
        }

        if ($request->id_thnpelajaran != '') {
            $where .= ' AND tp.id = ' . $request->id_thnpelajaran;
        }

        if ($request->tgl_awal != '' && $request->tgl_akhir) {
            $where .= " AND s.tgl_sispeling BETWEEN '$request->tgl_awal' AND '$request->tgl_akhir'";
        }

        $data = DB::select("SELECT s.tgl_sispeling,tp.nama_thnpelajaran,k.tingkat,k.nama_kelas,ds.nominal FROM sispeling s INNER JOIN detail_sispeling ds ON ds.id_sispeling = s.id INNER JOIN kelas k ON k.id = ds.id_kelas INNER JOIN thnpelajarans tp ON tp.id = k.id_thnpelajaran WHERE k.tingkat = $request->tingkat $where");

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('nominal', function ($row) {
                return 'Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $row->nominal);
            })
            ->addColumn('tingkat', function ($row) {

                $tingkat = '';
                if ($row->tingkat == 1) {
                    $tingkat = 'X (Sepuluh)';
                } else if ($row->tingkat == 2) {
                    $tingkat = 'XI (Sebelas)';
                } else if ($row->tingkat == 3) {
                    $tingkat = 'XII (Dua Belas)';
                }
                return $tingkat;
            })
            ->rawColumns(['tingkat'])
            ->make(true);
    }

    public function cetakPdf(Request $request)
    {
        $where = '';
        if (isset($request->kelas) && $request->kelas != '') {
            $where .= ' AND k.id = ' . $request->kelas;
        }

        if (isset($request->id_thnpelajaran) && $request->id_thnpelajaran != '') {
            $where .= ' AND tp.id = ' . $request->id_thnpelajaran;
        }

        if (isset($request->tgl_awal) && isset($request->tgl_akhir)) {
            if ($request->tgl_awal != 0 && $request->tgl_akhir != 0) {
                $where .= " AND s.tgl_sispeling = '$request->tgl_awal'";
                $where .= " AND s.tgl_sispeling <= '$request->tgl_akhir'";
            }
        } else {
            $date = date('Y-m-d');
            $where .= " AND s.tgl_sispeling = '$date'";
        }

        $data = DB::select("SELECT s.tgl_sispeling,tp.nama_thnpelajaran,k.tingkat,k.nama_kelas,ds.nominal FROM sispeling s INNER JOIN detail_sispeling ds ON ds.id_sispeling = s.id INNER JOIN kelas k ON k.id = ds.id_kelas INNER JOIN thnpelajarans tp ON tp.id = k.id_thnpelajaran WHERE k.tingkat = $request->tingkat $where");

        $total = DB::select("SELECT SUM(ds.nominal) as total FROM sispeling s INNER JOIN detail_sispeling ds ON ds.id_sispeling = s.id INNER JOIN kelas k ON k.id = ds.id_kelas INNER JOIN thnpelajarans tp ON tp.id = k.id_thnpelajaran WHERE k.tingkat = $request->tingkat $where");

        $total = isset($total[0]->total) ? $total[0]->total : 0;

        $pdf = PDF::loadView('sispeling.sispeling_pdf', ['datas' => $data, 'total' => $total]);
        $path = public_path('pdf/');
        $fileName =  time() . '.' . 'pdf';
        $pdf->save($path . '/' . $fileName);
        $pdf = public_path('pdf/' . $fileName);

        return response()->download($pdf);
    }
}
