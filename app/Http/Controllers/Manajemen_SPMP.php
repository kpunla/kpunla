<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use PDF;

class Manajemen_SPMP extends Controller
{
    public function index()
    {
        $siswa = DB::table('siswas')->join('pengajuan_spmp', 'pengajuan_spmp.id_siswa', 'siswas.id')->join('alokasi_kelas', 'alokasi_kelas.id_siswa', 'siswas.id')->join('status', 'status.id', '=', 'pengajuan_spmp.id_status')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->select('*', 'pengajuan_spmp.id as id_pengajuan_spmp')->orderBy('pengajuan_spmp.id', 'desc')->get();

        $arr = [
            'menu' => 'Sumbangan Dana Pendidikan/Siswa',
            // 'jurusans' => Jurusan::all(),
            'thnajarans' => DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->orderBy('id', 'desc')->get(),
            'siswas' => $siswa,
        ];

        return view('spmp.management.index', $arr);
    }

    public function DTManajemenSPMP(Request $request)
    {
        $where = '';
        if (isset($request->id_thnpelajaran) && $request->id_thnpelajaran != '') {
            $where .= ' WHERE k.id_thnpelajaran = ' . $request->id_thnpelajaran;
        }

        if (isset($request->tingkat) && $request->tingkat != '') {
            $where .= ' AND k.tingkat = ' . $request->tingkat;
        }

        if (isset($request->kelas) && $request->kelas != '') {
            $where .= ' AND k.id = ' . $request->kelas;
        }

        if (isset($request->nmjalurmasuk) && $request->nmjalurmasuk != '') {
            $where .= " AND s.nmjalurmasuk = '$request->nmjalurmasuk'";
        }

        if ($request->status_pengajuan == '') {
            $where .= " AND st.id IS NULL";
        } else {
            if (isset($request->status_pengajuan) && $request->status_pengajuan != 'ALL') {
                $where .= " AND st.id = '$request->status_pengajuan'";
            }
        }

        if (isset($request->status_pembayaran) && $request->status_pembayaran != '') {
            $where .= " AND pbs.status_pembayaran = '$request->status_pembayaran'";
        }

        $data = DB::select("SELECT s.nis,s.nmlengkap,k.nama_kelas,s.nmjalurmasuk,st.nama_status as status_pengajuan,st.id as id_status_pengajuan,pbs.status_pembayaran,k.tingkat,k.id,ps.id as id_pengajuan_spmp,ps.id_status FROM siswas s LEFT JOIN pengajuan_spmp ps ON s.id = ps.id_siswa LEFT JOIN pembayaran_spmp pbs ON pbs.id_siswa = s.id JOIN(SELECT * FROM alokasi_kelas WHERE alokasi_kelas.is_active = 1) ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas LEFT JOIN status st ON st.id = ps.id_status $where GROUP BY s.nis;");

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('status_pengajuan', function ($row) {
                if ($row->id_status_pengajuan == 0) {
                    $bg = 'danger';
                } else if ($row->id_status_pengajuan == 1) {
                    $bg = 'warning';
                } else if ($row->id_status_pengajuan == 2) {
                    $bg = 'success';
                } else {
                    $bg = 'danger';
                }

                if ($row->id_status_pengajuan != '' || $row->id_status_pengajuan === 0) {
                    $btn = '<span class="badge bg-' . $bg . '">' . $row->status_pengajuan . '</span>';
                } else {
                    $btn = '<span class="badge bg-' . $bg . '">Belum Mengajukan</span>';
                }
                return $btn;
            })
            ->addColumn('status_pembayaran', function ($row) {
                if ($row->status_pembayaran != '') {
                    if ($row->status_pembayaran == 'LUNAS') {
                        $bg = 'success';
                    } else {
                        $bg = 'danger';
                    }
                    $btn = '<span class="badge bg-' . $bg . '">' . $row->status_pembayaran . '</span>';
                } else {
                    $bg = 'danger';
                    $btn = '<span class="badge bg-' . $bg . '">Belum ada pembayaran</span>';
                }

                return $btn;
            })
            ->addColumn('action', function ($row) {
                if ($row->id_status_pengajuan == 2) {
                    $btn = '<a href="/spmp/transaksi/' . $row->nis . '" class="btn icon btn-success">
                            Transaksi <i class="fa fa-chevron-right"></i>
                        </a>';
                } elseif ($row->id_status_pengajuan == 1) {
                    $btn = '<button class="btn btn-success btn-approval" data-id="' . $row->id_pengajuan_spmp . '">Approve</button>';
                } else {
                    $btn = '<a href="/spmp/pengajuan/' . $row->nis . '" class="btn icon btn-warning">
                            Ajukan <i class="fa fa-chevron-right"></i>
                        </a>';
                }
                return $btn;
            })
            ->rawColumns(['action', 'status_pengajuan', 'status_pembayaran'])
            ->make(true);
    }

    public function cetakPdf(Request $request)
    {
        $where = '';
        if (isset($request->id_thnpelajaran) && $request->id_thnpelajaran != '') {
            $where .= ' WHERE k.id_thnpelajaran = ' . $request->id_thnpelajaran;
        }

        if (isset($request->tingkat) && $request->tingkat != '') {
            $where .= ' AND k.tingkat = ' . $request->tingkat;
        }

        if (isset($request->kelas) && $request->kelas != '') {
            $where .= ' AND k.id = ' . $request->kelas;
        }

        if (isset($request->nmjalurmasuk) && $request->nmjalurmasuk != '') {
            $where .= " AND s.nmjalurmasuk = '$request->nmjalurmasuk'";
        }

        if ($request->status_pengajuan == '') {
            $where .= " AND st.id IS NULL";
        } else {
            if (isset($request->status_pengajuan) && $request->status_pengajuan != 'ALL') {
                $where .= " AND st.id = '$request->status_pengajuan'";
            }
        }

        if (isset($request->status_pembayaran) && $request->status_pembayaran != '') {
            $where .= " AND pbs.status_pembayaran = '$request->status_pembayaran'";
        }

        $data = DB::select("SELECT s.id as id_siswa,s.nis,s.nmlengkap,k.nama_kelas,s.nmjalurmasuk,IFNULL(st.nama_status,'Belum Mengajukan') as status_pengajuan,IFNULL(st.id,3) as id_status_pengajuan,IFNULL(pbs.status_pembayaran,'BELUM LUNAS') as status_pembayaran FROM siswas s LEFT JOIN pengajuan_spmp ps ON s.id = ps.id_siswa LEFT JOIN pembayaran_spmp pbs ON pbs.id_siswa = s.id INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas LEFT JOIN status st ON st.id = ps.id_status $where");

        $pdf = PDF::loadView('spmp.management.spmp_pdf', ['datas' => $data]);
        $path = public_path('pdf/');
        $fileName =  time() . '.' . 'pdf';
        $pdf->save($path . '/' . $fileName);
        $pdf = public_path('pdf/' . $fileName);

        return response()->download($pdf);
    }
}
