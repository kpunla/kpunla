<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ThnpelajaranController extends Controller
{
    public function index()
    {
        return view('thnpelajaran.index', [
            'menu' => 'Manajemen Admin/Tahun Pelajaran',
        ]);
    }

    public function create()
    {
        return view('thnpelajaran.create', [
            'menu' => 'Manajemen Admin/Tahun Pelajaran/Form Tambah Tahun Pelajaran',
        ]);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama_thnpelajaran' => 'required|max:20',
        ]);

        $data = [
            'nama_thnpelajaran' => $request->nama_thnpelajaran,
            'status_thnpelajaran' => 1,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ];

        $action = DB::table('thnpelajarans')->insert($data);

        if ($action) {
            return redirect('/manajemen_admin/thnpelajaran')->with('success', 'Tahun Pelajaran berhasil ditambahkan');
        } else {
            return redirect('/manajemen_admin/thnpelajaran/create')->with('error', 'Tahun Pelajaran gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('thnpelajaran.edit', [
            'menu' => 'Manajemen Admin/Form Update Tahun Pelajaran',
            'thnpelajaran' => DB::table('thnpelajarans')->where('id', $id)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validateData = $request->validate([
            'nama_thnpelajaran' => 'required|unique:thnpelajarans|max:50',
        ]);

        $data = [
            'nama_thnpelajaran' => $request->nama_thnpelajaran,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ];

        $action = DB::table('thnpelajarans')->where('id', $id)->update($data);

        if ($action) {
            return redirect('/manajemen_admin/thnpelajaran')->with('success', 'Tahun Pelajaran berhasil diubah');
        } else {
            return redirect('/manajemen_admin/thnpelajaran/create')->with('error', 'Tahun Pelajaran gagal diubah');
        }
    }

    public function datatable()
    {
        $data = DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="/manajemen_admin/thnpelajaran/' . $row->id . '/edit" class="btn icon btn-warning">
                             <i class="fa fa-pencil-alt"></i>
                         </a>';
                $btn .= '<button style="margin-left:2px;" data-id="' . $row->id . '" class="btn icon btn-danger btn-delete">
                             <i class="fa fa-trash"></i>
                         </button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function destroy($id)
    {
        $action = DB::table('thnpelajarans')->where('id', $id)->update(['status_thnpelajaran' => 0]);

        if ($action) {
            echo json_encode(['msg' => 'Tahun Pelajaran berhasil dihapus', 'error' => false]);
        } else {
            echo json_encode(['msg' => 'Tahun Pelajaran gagal dihapus', 'error' => true]);
        }
    }
}
