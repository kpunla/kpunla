<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\Thnpelajaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use PDF;

class TransaksiSPMPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('spmp.transaksi.create_transaksi', [
            'menu' => 'Sumbangan Dana Pendidikan/Pengajuan Sumbangan Dana Pendidikan',
            'thnpelajarans' => Thnpelajaran::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query_pembayaran_spmp = DB::table('pembayaran_spmp')->where('id_siswa', $request->id_siswa)->get();
        $query_no_pembayaran = DB::table('pembayaran_spmp')->where('pembayaran_spmp.id_siswa', $request->id_siswa)->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->count();
        $no_pembayaran = $query_no_pembayaran + 1;

        // ================ check total bayar =====================================================================================
        $bayar = str_replace(".", "", $request->bayar);

        if (intval(str_replace(".", "", $request->kembalian)) > 0) {
            $bayar = intval(str_replace(".", "", $request->bayar)) - intval(str_replace(".", "", $request->kembalian));
        }
        // ================ akhir check total bayar =====================================================================================


        // check lunas atau tidak ================================
        $total_pembayaran = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('pembayaran_spmp.id', DB::raw('SUM(detail_pembayaran_spmp.bayar) as total'))->groupBy('pembayaran_spmp.id')->where('id_siswa', $request->id_siswa)->get();

        if (isset($total_pembayaran[0]->total)) {
            $sisa = intval($query_pembayaran_spmp[0]->total_tagihan) - intval($total_pembayaran[0]->total);
            $hasil = $sisa - $bayar;
        } else {
            $hasil = $bayar;
        }
        // akhir check lunas atau tidak ================================


        $data_detail['id_pembayaran_spmp'] = $query_pembayaran_spmp[0]->id;

        if ($hasil == 0) {
            DB::table('pembayaran_spmp')->where('id', $data_detail['id_pembayaran_spmp'])->update(['status_pembayaran' => 'LUNAS']);


            // insert history ==================
            $dataPembayaran['id_pengajuan_spmp'] = $query_pembayaran_spmp[0]->id_pengajuan_spmp;
            $dataPembayaran['id_siswa'] = $query_pembayaran_spmp[0]->id_siswa;
            $dataPembayaran['total_tagihan'] = $query_pembayaran_spmp[0]->total_tagihan;
            $dataPembayaran['status_pembayaran'] = 'LUNAS';
            $dataPembayaran['created_by'] = $request->session()->get('id');
            $dataPembayaran['created_date'] = date('Y-m-d h:i:s');
            $dataPembayaran['action'] = 'SUBMIT';
            $this->storeHistoryPembayaran($dataPembayaran);
            // insert history ==================
        }


        $data_detail['nama_pembayaran'] = 'spmp-' . $no_pembayaran . ' (' . date('F') . ').';
        $data_detail['id_kelas'] = $request->id_kelas;
        $data_detail['bayar'] = $bayar;
        $data_detail['tgl_bayar'] = date('Y-m-d h:i:s');
        $data_detail['status_bayar'] = 'Bayar';
        $data_detail['created_by'] = $request->session()->get('id');
        $data_detail['created_date'] = date('Y-m-d h:i:s');

        $detail_pembayaran_spmp = DB::table('detail_pembayaran_spmp')->insert($data_detail);

        if ($detail_pembayaran_spmp) {
            // insert history ==================
            $data_detail['action'] = 'SUBMIT';
            $this->storeHistoryDetailPembayaran($data_detail);
            // insert history ==================

            echo json_encode(['error' => false, 'msg' => 'Pembayaran berhasil ditambahkan']);
        } else {
            echo json_encode(['error' => true, 'msg' => 'Pembayaran gagal ditambahkan']);
        }
    }

    public function storeHistoryDetailPembayaran($data_detail)
    {
        DB::table('history_detail_pembayaran_spmp')->insert($data_detail);
    }

    public function storeHistoryPembayaran($dataPembayaran)
    {
        DB::table('history_pembayaran_spmp')->insert($dataPembayaran);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('spmp.transaksi.create_transaksi', [
            'menu' => 'Sumbangan Dana Pendidikan/Pengajuan Sumbangan Dana Pendidikan',
            'thnpelajarans' => Thnpelajaran::all(),
            'nis' => $id,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAll(Request $request)
    {
        $siswa = DB::table('siswas')->where('siswas.nis', $request->nis)->where('pengajuan_spmp.id_status', 2)->join('pengajuan_spmp', 'pengajuan_spmp.id_siswa', '=', 'siswas.id')->select('siswas.*', 'pengajuan_spmp.id as id_pengajuan_spmp', 'pengajuan_spmp.jml_diajukan')->get();

        $id_siswa = isset($siswa[0]->id) ? $siswa[0]->id : '';


        if ($id_siswa != '') {
            $getInformasiSiswa = $this->getInformasiSiswa($request->nis);

            $getTransaksiTerakhir = $this->getTransaksiTerakhir($id_siswa);

            $getPembayaran = $this->getPembayaran($id_siswa);

            $getInformasiPembayaran = $this->getInformasiPembayaran($id_siswa);

            echo json_encode(['error' => false, 'getInformasiSiswa' => $getInformasiSiswa['html'], 'id_siswa' => $id_siswa, 'id_kelas' => $getInformasiSiswa['id_kelas'], 'jml_diajukan' => $siswa[0]->jml_diajukan, 'id_pengajuan_spmp' => $siswa[0]->id_pengajuan_spmp, 'getTransaksiTerakhir' => $getTransaksiTerakhir, 'getPembayaran' => $getPembayaran, 'getInformasiPembayaran' => $getInformasiPembayaran]);
        } else {
            echo json_encode(['error' => true, 'html' => '<div class="text-center"><i class="text-primary fs-3 fa fa-exclamation-triangle"></i> <span class="text-primary fs-3"> Data Tidak Ditemukan</span></div>']);
        }
    }

    public function getInformasiSiswa($nis)
    {
        $siswa = DB::table('alokasi_kelas')->join('siswas', 'siswas.id', '=', 'alokasi_kelas.id_siswa')->join('kelas', 'kelas.id', '=', 'alokasi_kelas.id_kelas')->join('thnpelajarans', 'thnpelajarans.id', '=', 'kelas.id_thnpelajaran')->where('alokasi_kelas.is_active', 1)->where('siswas.nis', $nis)->get();

        $id_kelas = $siswa[0]->id_kelas;

        $html = '<table class="table table-striped">';
        if (isset($siswa[0]->nama_thnpelajaran)) {
            $html .= '<tr>
                        <th>Tahun Ajaran</th>
                        <th>' . $siswa[0]->nama_thnpelajaran . '</th>
                        </tr>
                        <tr>
                            <th>NIS</th>
                            <th>' . $siswa[0]->nis . '</th>
                        </tr>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>' . $siswa[0]->nmlengkap . '</th>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <th>' . $siswa[0]->nama_kelas . '</th>
                        </tr>
                        <tr>
                            <th>Jalur Masuk</th>
                            <th>' . $siswa[0]->nmjalurmasuk . '</th>
                        </tr>';
        } else {
            $html .= '<tr>
                        <th>Tahun Ajaran</th>
                        <th>-</th>
                        </tr>
                        <tr>
                            <th>NIS</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Nama Siswa</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <th>Jalur Masuk</th>
                            <th>-</th>
                        </tr>';
        }
        $html .= '</table>';
        return ['html' => $html, 'id_kelas' => $id_kelas];
    }

    public function getTransaksiTerakhir($id_siswa)
    {

        $html = '<table class="table table-striped">';
        $html .= '<tr class="bg-primary">
        <th class="text-white">Nama Pembayaran</th>
        <th class="text-white">Nominal</th>
        <th class="text-white">Tanggal</th>
        <th class="text-white">Action</th>
        </tr>';

        if ($id_siswa != '') {
            $trans = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('detail_pembayaran_spmp.*')->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

            $total_bayar = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select(DB::raw('SUM(detail_pembayaran_spmp.bayar) as total_bayar'))->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

            if (isset($trans[0]->id)) {
                foreach ($trans as $row) {
                    $html .= '<tr>';
                    $html .= '<th>' . $row->nama_pembayaran . '</th>';
                    $html .= '<th>Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $row->bayar) . '</th>';
                    $html .= '<th>' . $row->tgl_bayar . '</th>';
                    $html .= '<th><div class="btn-group">
                    <a href="#" class="btn btn-info btn-export-detail" data-id="' . $row->id . '" aria-current="page"><i class="fa fa-print"></i></a>
                    <a href="#" class="btn btn-warning btn-edit" data-id_detail_pembayaran="' . $row->id . '" data-nominal="' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $row->bayar) . '" aria-current="page"><i class="fa fa-pencil-alt"></i></a>
                    <a href="#" class="btn btn-danger btn-delete-detail" data-id="' . $row->id . '"><i class="fa fa-trash"></i></a>
                </div></th>';
                    $html .= '</tr>';
                }
                $html .= '<tr class="bg-primary"><th class="text-white">Total</th><th colspan="3" class="text-white">Rp.' . preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $total_bayar[0]->total_bayar) . '</th></tr>';
            } else {
                $html .= '<tr>
                                <th colspan="4" class="text-center">No Data</th>
                            </tr>';
            }
        } else {
            $html .= '<tr>
                        <th>-</th>
                        <th>-</th>
                        <th>-</th>
                    </tr>';
        }

        $html .= '</table>';

        return $html;
    }

    public function getPembayaran($id_siswa)
    {
        $query = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

        $html = '<table class="table table-bordered">
                    <tr class="bg-primary">
                        <th class="text-white">No</th>
                        <th class="text-white">Nama Pembayaran</th>
                        <th class="text-white">Sisa Tagihan</th>
                        <th class="text-white">Juli</th>
                        <th class="text-white">Agustus</th>
                        <th class="text-white">September</th>
                        <th class="text-white">Oktober</th>
                        <th class="text-white">November</th>
                        <th class="text-white">Desember</th>
                        <th class="text-white">Januari</th>
                        <th class="text-white">Februari</th>
                        <th class="text-white">Maret</th>
                        <th class="text-white">April</th>
                        <th class="text-white">Mei</th>
                        <th class="text-white">Juni</th>
                    </tr>';

        $html .= '  <tr>
                    <th>1</th>
                    <th>SDP - 2022/2021</th>
                    <th>Rp.2000.000</th>';


        $html .= '<th><a href="#" id="btn-modal-bayar" data-bs-toggle="modal" data-bs-target="#modal-bayar" class="badge bg-danger">Bayar</a></th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>';

        return $html;
    }

    public function getInformasiPembayaran($id_siswa)
    {

        $this->updateStatusPembayaran($id_siswa);

        $query = DB::table('pembayaran_spmp')->join('pengajuan_spmp', 'pengajuan_spmp.id', '=', 'pembayaran_spmp.id_pengajuan_spmp')->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

        $total_tagihan = $query[0]->total_tagihan;
        $file_pengajuan = $query[0]->file_surat_pernyataan;
        $status_pembayaran = $query[0]->status_pembayaran;

        $bayar = DB::table('detail_pembayaran_spmp')->join('pembayaran_spmp', 'pembayaran_spmp.id', '=', 'detail_pembayaran_spmp.id_pembayaran_spmp')->select(DB::raw('SUM(bayar) as total_bayar', ''))->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

        if ($bayar) {
            $sisa_tagihan = intval($query[0]->total_tagihan) - intval($bayar[0]->total_bayar);
        } else {
            $sisa_tagihan = intval($query[0]->total_tagihan);
        }

        return ['total_tagihan' => preg_replace('/(?!^)(?=(?:\d{3})+$)/m', '.', $total_tagihan), 'file_pengajuan' => '<button class="btn btn-primary" id="btn-file-pengajuan">Lihat File</button> ', 'status_pembayaran' => $status_pembayaran, 'sisa_tagihan' => $sisa_tagihan, 'img_pengajuan' => '<img src="' . asset('storage/' . $file_pengajuan) . '" class="w-100 img-thumbnail" id="img-pengajuan" alt="">'];
    }

    public function updateStatusPembayaran($id_siswa)
    {
        $query_pembayaran_spmp = DB::table('pembayaran_spmp')->where('id_siswa', $id_siswa)->get();
        $total_pembayaran = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('pembayaran_spmp.id', DB::raw('SUM(detail_pembayaran_spmp.bayar) as total'))->groupBy('pembayaran_spmp.id')->where('id_siswa', $id_siswa)->get();

        if (isset($total_pembayaran[0]->total)) {
            $hasil = intval($query_pembayaran_spmp[0]->total_tagihan) - intval($total_pembayaran[0]->total);
        } else {
            // jika baru mengajukan
            $hasil = 1;
        }

        if ($hasil == 0) {
            DB::table('pembayaran_spmp')->where('id_siswa', $id_siswa)->update(['status_pembayaran' => 'LUNAS']);
        } else {
            DB::table('pembayaran_spmp')->where('id_siswa', $id_siswa)->update(['status_pembayaran' => 'BELUM LUNAS']);
        }
    }

    public function cetakAllPdf(Request $request)
    {
        $nis = $request->nis;

        $q_siswa = DB::table('siswas')->where('nis', $nis)->get();

        if (isset($q_siswa[0]->id)) {
            $id_siswa = $q_siswa[0]->id;
            $trans = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('detail_pembayaran_spmp.*')->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

            $total_bayar = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select(DB::raw('SUM(detail_pembayaran_spmp.bayar) as total_bayar'))->where('pembayaran_spmp.id_siswa', $id_siswa)->get();

            $pdf = PDF::loadView('spmp.transaksi.spmp_transaksi_pdf', ['trans' => $trans, 'total_bayar' => $total_bayar]);
            $path = public_path('pdf/');
            $fileName =  time() . '.' . 'pdf';
            $pdf->save($path . '/' . $fileName);
            $pdf = public_path('pdf/' . $fileName);

            return response()->download($pdf);
        }
    }

    public function cetakDetailPdf(Request $request)
    {
        $nis = $request->nis;

        $q_siswa = DB::table('siswas')->where('nis', $nis)->get();

        if (isset($q_siswa[0]->id)) {
            $id_siswa = $q_siswa[0]->id;
            $trans = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select('detail_pembayaran_spmp.*')->where('pembayaran_spmp.id_siswa', $id_siswa)->where('detail_pembayaran_spmp.id', $request->id)->get();

            $total_bayar = DB::table('pembayaran_spmp')->join('detail_pembayaran_spmp', 'detail_pembayaran_spmp.id_pembayaran_spmp', '=', 'pembayaran_spmp.id')->select(DB::raw('SUM(detail_pembayaran_spmp.bayar) as total_bayar'))->where('pembayaran_spmp.id_siswa', $id_siswa)->where('detail_pembayaran_spmp.id', $request->id)->get();

            $pdf = PDF::loadView('spmp.transaksi.spmp_transaksi_pdf', ['trans' => $trans, 'total_bayar' => $total_bayar]);
            $path = public_path('pdf/');
            $fileName =  time() . '.' . 'pdf';
            $pdf->save($path . '/' . $fileName);
            $pdf = public_path('pdf/' . $fileName);

            return response()->download($pdf);
        }
    }

    public function editNominal(Request $request)
    {
        $q = DB::select("SELECT SUM(bayar) as sisa FROM `detail_pembayaran_spmp` INNER JOIN pembayaran_spmp ON pembayaran_spmp.id = detail_pembayaran_spmp.id_pembayaran_spmp WHERE detail_pembayaran_spmp.id != $request->id_detail_pembayaran AND id_siswa = $request->id_siswa_edit");

        if (isset($q[0]->sisa)) {
            $sisa = 0;
            foreach ($q as $e) {
                $sisa += $e->sisa;
            }

            $total = intval(str_replace(".", "", $request->total_tagihan_val));

            $hasil = $total - $sisa;
            $bayar = intval(str_replace(".", "", $request->editnominal));

            if ($bayar > $hasil) {
                echo json_encode(['total' => $total, 'sisa' => $sisa, 'hasil' => $hasil, 'bayar' => $bayar, 'error' => true, 'nominal_err' => 'Nominal melebihi sisa tagihan', 'msg' => 'Nominal gagal berhasil diubah']);
                exit;
            }
        }

        $query = DB::table('detail_pembayaran_spmp')->where('id', $request->id_detail_pembayaran)->update(['bayar' => intval(str_replace(".", "", $request->editnominal))]);

        if ($query) {
            echo json_encode(['error' => false, 'msg' => 'Nominal berhasil diubah']);
        } else {
            echo json_encode(['error' => true, 'msg' => 'Nominal gagal berhasil diubah']);
        }
    }

    public function deleteDetail(Request $request)
    {
        $id = $request->id;

        $s = DB::table('detail_pembayaran_spmp')->where('id', $id)->get();
        // $q = true;
        $q = DB::table('detail_pembayaran_spmp')->where('id', $id)->delete();

        if ($q) {
            $data_detail['nama_pembayaran'] = $s[0]->nama_pembayaran;
            $data_detail['bayar'] = $s[0]->bayar;
            $data_detail['tgl_bayar'] = $s[0]->tgl_bayar;
            $data_detail['status_bayar'] = $s[0]->status_bayar;
            $data_detail['created_by'] = $request->session()->get('id');
            $data_detail['created_date'] = date('Y-m-d h:i:s');
            $data_detail['action'] = 'DELETE';
            $this->storeHistoryDetailPembayaran($data_detail);
            echo json_encode(['error' => false, 'msg' => 'Data berhasil di delete']);
        } else {
            echo json_encode(['error' => true, 'msg' => 'Data gagal berhasil di delete']);
        }
    }
}
