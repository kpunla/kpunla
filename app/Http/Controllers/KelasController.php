<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class KelasController extends Controller
{
    public function index()
    {
        return view('kelas.index', [
            'menu' => 'Manajemen Admin/Kelas',
        ]);
    }

    public function create()
    {
        return view('kelas.create', [
            'menu' => 'Manajemen Admin/Kelas/Form Tambah Kelas',
            'thnpelajarans' => DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->get()
        ]);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'id_thnpelajaran' => 'required|max:20',
            'nama_kelas' => 'required|max:50',
            'walikelas' => 'required|max:50',
            'tingkat' => 'required|max:50',
            'total_siswa' => 'required|max:50',
        ]);

        $data = [
            'id_thnpelajaran' => $request->id_thnpelajaran,
            'nama_kelas' => $request->nama_kelas,
            'walikelas' => $request->walikelas,
            'tingkat' => $request->tingkat,
            'total_siswa' => $request->total_siswa,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ];

        $action = DB::table('kelas')->insert($data);

        if ($action) {
            return redirect('/manajemen_admin/kelas')->with('success', 'Kelas berhasil ditambahkan');
        } else {
            return redirect('/manajemen_admin/kelas/create')->with('error', 'Kelas gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('kelas.edit', [
            'menu' => 'Manajemen Admin/Form Edit Kelas',
            'kelas' => DB::table('kelas')->where('id', $id)->get(),
            'thnpelajarans' => DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hidden_id_thnpelajaran == $request->id_thnpelajaran) {
            $validateData = $request->validate([
                'nama_kelas' => 'required|max:50',
                'id_thnpelajaran' => 'required|max:20',
                'walikelas' => 'required|max:50',
                'tingkat' => 'required|max:50',
                'total_siswa' => 'required|max:50',
            ]);
        } else {
            $validateData = $request->validate([
                'nama_kelas' => 'required|unique:kelas|max:50',
                'id_thnpelajaran' => 'required|max:20',
                'walikelas' => 'required|max:50',
                'tingkat' => 'required|max:50',
                'total_siswa' => 'required|max:50',
            ]);
        }

        $data = [
            'nama_kelas' => $request->nama_kelas,
            'id_thnpelajaran' => $request->id_thnpelajaran,
            'walikelas' => $request->walikelas,
            'tingkat' => $request->tingkat,
            'total_siswa' => $request->total_siswa,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ];

        $action = DB::table('kelas')->where('id', $id)->update($data);

        if ($action) {
            return redirect('/manajemen_admin/kelas')->with('success', 'Kelas berhasil diubah');
        } else {
            return redirect('/manajemen_admin/kelas/create')->with('error', 'Kelas gagal diubah');
        }
    }

    public function datatable()
    {
        $data = DB::table('kelas')->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="/manajemen_admin/kelas/' . $row->id . '/edit" class="btn icon btn-warning">
                             <i class="fa fa-pencil-alt"></i>
                         </a>';
                $btn .= '<button style="margin-left:2px;" data-id="' . $row->id . '" class="btn icon btn-danger btn-delete">
                             <i class="fa fa-trash"></i>
                         </button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function destroy($id)
    {
        $action = DB::table('kelas')->where('id', $id)->delete();

        if ($action) {
            echo json_encode(['msg' => 'Kelas berhasil dihapus', 'error' => false]);
        } else {
            echo json_encode(['msg' => 'Kelas gagal dihapus', 'error' => true]);
        }
    }
}
