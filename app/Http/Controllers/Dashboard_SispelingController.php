<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Dashboard_SispelingController extends Controller
{
    public function index()
    {
        $thnajarans = DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->orderBy('id', 'desc')->get();
        return view('dashboard.sispeling.dashboard_sispeling', [
            'menu' => 'Dashboard/Sispeling',
            'thnajarans' => $thnajarans,
        ]);
    }

    public function getTotal(Request $request)
    {
        $where = '';
        if ($request->tgl_awal != '' && $request->tgl_akhir) {
            $where .= " AND s.tgl_sispeling BETWEEN '$request->tgl_awal' AND '$request->tgl_akhir'";
        }


        $data1 = DB::select("SELECT SUM(ds.nominal) as nominal FROM `sispeling` s INNER JOIN detail_sispeling ds ON ds.id_sispeling = s.id INNER JOIN kelas k ON k.id = ds.id_kelas WHERE s.tingkat = 1 AND k.id_thnpelajaran = $request->id_thnpelajaran $where");
        $data2 = DB::select("SELECT SUM(ds.nominal) as nominal FROM `sispeling` s INNER JOIN detail_sispeling ds ON ds.id_sispeling = s.id INNER JOIN kelas k ON k.id = ds.id_kelas WHERE s.tingkat = 2 AND k.id_thnpelajaran = $request->id_thnpelajaran $where");
        $data3 = DB::select("SELECT SUM(ds.nominal) as nominal FROM `sispeling` s INNER JOIN detail_sispeling ds ON ds.id_sispeling = s.id INNER JOIN kelas k ON k.id = ds.id_kelas WHERE s.tingkat = 3 AND k.id_thnpelajaran = $request->id_thnpelajaran $where");

        $nominal1 = isset($data1[0]->nominal) ? $data1[0]->nominal : 0;
        $nominal2 = isset($data2[0]->nominal) ? $data2[0]->nominal : 0;
        $nominal3 = isset($data3[0]->nominal) ? $data3[0]->nominal : 0;


        echo json_encode(['nominal1' => $nominal1, 'nominal2' => $nominal2, 'nominal3' => $nominal3]);
    }

    public function columnChart(Request $request)
    {
        $where = '';
        // if ($request->tgl_awal != '' && $request->tgl_akhir) {
        //     $where .= " AND s.tgl_sispeling BETWEEN '$request->tgl_awal' AND '$request->tgl_akhir'";
        // }

        $tingkat1 = DB::select("SELECT SUM(ds.nominal) as nominal, MONTHNAME(s.tgl_sispeling) as month FROM sispeling s INNER JOIN detail_sispeling ds ON s.id = ds.id_sispeling INNER JOIN kelas k ON k.id = ds.id_kelas WHERE k.tingkat = 1 AND k.id_thnpelajaran = $request->id_thnpelajaran $where GROUP BY YEAR(s.tgl_sispeling),MONTH(s.tgl_sispeling)");

        $tingkat2 = DB::select("SELECT SUM(ds.nominal) as nominal, MONTHNAME(s.tgl_sispeling) as month FROM sispeling s INNER JOIN detail_sispeling ds ON s.id = ds.id_sispeling INNER JOIN kelas k ON k.id = ds.id_kelas WHERE k.tingkat = 2 AND k.id_thnpelajaran = $request->id_thnpelajaran $where GROUP BY YEAR(s.tgl_sispeling),MONTH(s.tgl_sispeling)");

        $tingkat3 = DB::select("SELECT SUM(ds.nominal) as nominal, MONTHNAME(s.tgl_sispeling) as month FROM sispeling s INNER JOIN detail_sispeling ds ON s.id = ds.id_sispeling INNER JOIN kelas k ON k.id = ds.id_kelas WHERE k.tingkat = 3 AND k.id_thnpelajaran = $request->id_thnpelajaran $where GROUP BY YEAR(s.tgl_sispeling),MONTH(s.tgl_sispeling)");


        $arr1 = ['January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0];
        $arr2 = ['January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0];
        $arr3 = ['January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0];

        foreach ($tingkat1 as $key => $t1) {
            if (in_array($t1->month, $arr1)) {
                $arr1[$t1->month] = $t1->nominal;
            }
        }

        foreach ($tingkat2 as $key => $t2) {
            if (in_array($t2->month, $arr2)) {
                $arr2[$t2->month] = $t2->nominal;
            }
        }

        foreach ($tingkat3 as $key => $t3) {
            if (in_array($t3->month, $arr3)) {
                $arr3[$t3->month] = $t3->nominal;
            }
        }


        echo json_encode(['tingkat1' => $arr1, 'tingkat2' => $arr2, 'tingkat3' => $arr3]);
    }
}
