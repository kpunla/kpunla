<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Dashboard_SPMPController extends Controller
{
    public function index()
    {
        return view('dashboard.sdp.dashboard_sdp', [
            'menu' => 'Dashboard/Sumbangan Peningkatan Mutu Pendidikan',
            'thnajarans' => DB::table('thnpelajarans')->where('status_thnpelajaran', 1)->orderBy('id', 'desc')->get(),
        ]);
    }

    public function dashboard(Request $request)
    {
        $id_thnpelajaran = $request->id_thnpelajaran;

        echo json_encode(['total' => $this->getTotal($id_thnpelajaran), 'chart1' => $this->chart1($id_thnpelajaran), 'chart2' => $this->chart2($id_thnpelajaran)]);
    }

    public function chart1($id_thnpelajaran)
    {
        $totalx = DB::select("SELECT pes.status_pembayaran FROM pembayaran_spmp pes RIGHT JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active = 1 AND k.tingkat = 1 AND k.id_thnpelajaran = $id_thnpelajaran");

        $totalxLunas = 0;
        $totalxBelumLunas = 0;

        foreach ($totalx as $t) {
            if ($t->status_pembayaran == 'LUNAS') {
                $totalxLunas++;
            } else {
                $totalxBelumLunas++;
            }
        }

        $totalxi = DB::select("SELECT pes.status_pembayaran FROM pembayaran_spmp pes RIGHT JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active = 1 AND k.tingkat = 2 AND k.id_thnpelajaran = $id_thnpelajaran");

        $totalxiLunas = 0;
        $totalxiBelumLunas = 0;

        foreach ($totalxi as $t) {
            if ($t->status_pembayaran == 'LUNAS') {
                $totalxiLunas++;
            } else {
                $totalxiBelumLunas++;
            }
        }

        $totalxii = DB::select("SELECT pes.status_pembayaran FROM pembayaran_spmp pes RIGHT JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active = 1 AND k.tingkat = 3 AND k.id_thnpelajaran = $id_thnpelajaran");

        $totalxiiLunas = 0;
        $totalxiiBelumLunas = 0;

        foreach ($totalxii as $t) {
            if ($t->status_pembayaran == 'LUNAS') {
                $totalxiiLunas++;
            } else {
                $totalxiiBelumLunas++;
            }
        }

        $result['X']['lunas'] = $totalxLunas;
        $result['X']['belum_lunas'] = $totalxBelumLunas;

        $result['XI']['lunas'] = $totalxiLunas;
        $result['XI']['belum_lunas'] = $totalxiBelumLunas;

        $result['XII']['lunas'] = $totalxiiLunas;
        $result['XII']['belum_lunas'] = $totalxiiBelumLunas;

        return $result;
    }

    public function chart2($id_thnpelajaran)
    {
        $qx = DB::select("SELECT IFNULL(pe.id_status,3) as id_status FROM pengajuan_spmp pe RIGHT JOIN siswas s ON s.id = pe.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active = 1 AND k.tingkat = 1 AND k.id_thnpelajaran = $id_thnpelajaran");

        $belumx = 0;
        $sedangx = 0;
        $tolakx = 0;
        $setujux = 0;

        foreach ($qx as $r) {
            if ($r->id_status == 0) {
                $tolakx++;
            } else if ($r->id_status == 1) {
                $sedangx++;
            } else if ($r->id_status == 2) {
                $setujux++;
            } else {
                $belumx++;
            }
        }

        $result['X']['belum'] = $belumx;
        $result['X']['sedang'] = $sedangx;
        $result['X']['tolak'] = $tolakx;
        $result['X']['setuju'] = $setujux;

        $qxi = DB::select("SELECT IFNULL(pe.id_status,3) as id_status FROM pengajuan_spmp pe RIGHT JOIN siswas s ON s.id = pe.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active = 1 AND k.tingkat = 2 AND k.id_thnpelajaran = $id_thnpelajaran");

        $belumxi = 0;
        $sedangxi = 0;
        $tolakxi = 0;
        $setujuxi = 0;

        foreach ($qxi as $r) {
            if ($r->id_status == 0) {
                $tolakxi++;
            } else if ($r->id_status == 1) {
                $sedangxi++;
            } else if ($r->id_status == 2) {
                $setujuxi++;
            } else {
                $belumxi++;
            }
        }

        $result['XI']['belum'] = $belumxi;
        $result['XI']['sedang'] = $sedangxi;
        $result['XI']['tolak'] = $tolakxi;
        $result['XI']['setuju'] = $setujuxi;

        $qxi = DB::select("SELECT IFNULL(pe.id_status,3) as id_status FROM pengajuan_spmp pe RIGHT JOIN siswas s ON s.id = pe.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active = 1 AND k.tingkat = 3 AND k.id_thnpelajaran = $id_thnpelajaran");

        $belumxii = 0;
        $sedangxii = 0;
        $tolakxii = 0;
        $setujuxii = 0;

        foreach ($qxi as $r) {
            if ($r->id_status == 0) {
                $tolakxii++;
            } else if ($r->id_status == 1) {
                $sedangxii++;
            } else if ($r->id_status == 2) {
                $setujuxii++;
            } else {
                $belumxii++;
            }
        }

        $result['XII']['belum'] = $belumxii;
        $result['XII']['sedang'] = $sedangxii;
        $result['XII']['tolak'] = $tolakxii;
        $result['XII']['setuju'] = $setujuxii;

        return $result;
    }

    public function getTotal($id_thnpelajaran)
    {
        $q_totalx = DB::select("SELECT SUM(pes.total_tagihan) as total_tagihan FROM pembayaran_spmp pes INNER JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active AND k.tingkat = 1 AND k.id_thnpelajaran = $id_thnpelajaran");
        $q_totalxi = DB::select("SELECT SUM(pes.total_tagihan) as total_tagihan FROM pembayaran_spmp pes INNER JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active AND k.tingkat = 2 AND k.id_thnpelajaran = $id_thnpelajaran");
        $q_totalxii = DB::select("SELECT SUM(pes.total_tagihan) as total_tagihan FROM pembayaran_spmp pes INNER JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active AND k.tingkat = 3 AND k.id_thnpelajaran = $id_thnpelajaran");

        $q_bayarx = DB::select("SELECT SUM(dp.bayar) as bayar FROM detail_pembayaran_spmp dp INNER JOIN pembayaran_spmp pes ON pes.id = dp.id_pembayaran_spmp INNER JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active AND k.tingkat = 1 AND k.id_thnpelajaran = $id_thnpelajaran");
        $q_bayarxi = DB::select("SELECT SUM(dp.bayar) as bayar FROM detail_pembayaran_spmp dp INNER JOIN pembayaran_spmp pes ON pes.id = dp.id_pembayaran_spmp INNER JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active AND k.tingkat = 2 AND k.id_thnpelajaran = $id_thnpelajaran");
        $q_bayarxii = DB::select("SELECT SUM(dp.bayar) as bayar FROM detail_pembayaran_spmp dp INNER JOIN pembayaran_spmp pes ON pes.id = dp.id_pembayaran_spmp INNER JOIN siswas s ON s.id = pes.id_siswa INNER JOIN alokasi_kelas ak ON ak.id_siswa = s.id INNER JOIN kelas k ON k.id = ak.id_kelas WHERE ak.is_active AND k.tingkat = 3 AND k.id_thnpelajaran = $id_thnpelajaran");

        $totalx = 0;
        $totalxi = 0;
        $totalxii = 0;

        $totalMasukx = 0;
        $totalMasukxi = 0;
        $totalMasukxii = 0;

        if (isset($q_totalx[0]->total_tagihan) && $q_totalx[0]->total_tagihan != '') {
            $totalx = $q_totalx[0]->total_tagihan;
        }

        if (isset($q_totalxi[0]->total_tagihan) && $q_totalxi[0]->total_tagihan != '') {
            $totalxi = $q_totalxi[0]->total_tagihan;
        }

        if (isset($q_totalxii[0]->total_tagihan) && $q_totalxii[0]->total_tagihan != '') {
            $totalxii = $q_totalxii[0]->total_tagihan;
        }

        if (isset($q_bayarx[0]->bayar) && $q_bayarx[0]->bayar != '') {
            $totalMasukx = $q_bayarx[0]->bayar;
        }

        if (isset($q_bayarxi[0]->bayar) && $q_bayarxi[0]->bayar != '') {
            $totalMasukxi = $q_bayarxi[0]->bayar;
        }

        if (isset($q_bayarxii[0]->bayar) && $q_bayarxii[0]->bayar != '') {
            $totalMasukxii = $q_bayarxii[0]->bayar;
        }

        $sisax = $totalx - $totalMasukx;
        $sisaxi = $totalxi - $totalMasukxi;
        $sisaxii = $totalxii - $totalMasukxii;

        $result['X']['total'] = $totalx;
        $result['X']['masuk'] = $totalMasukx;
        $result['X']['sisa'] = $sisax;

        $result['XI']['total'] = $totalxi;
        $result['XI']['masuk'] = $totalMasukxi;
        $result['XI']['sisa'] = $sisaxi;

        $result['XII']['total'] = $totalxii;
        $result['XII']['masuk'] = $totalMasukxii;
        $result['XII']['sisa'] = $sisaxii;

        return $result;
    }
}
