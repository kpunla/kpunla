<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function checklogin(Request $request)
    {
        $validateData = $request->validate([
            'username' => 'required|max:50',
            'password' => 'required|max:15',
        ]);

        $users = DB::table('users')
            ->where('username', '=', $request->username)
            ->get();

        // check password
        if (isset($users[0]->password)) {
            if (Crypt::decryptString($users[0]->password) == $request->password) {
                $role = DB::table('roles')
                    ->where('id', '=', $users[0]->id_role)
                    ->get();

                $request->session()->put('id', $users[0]->id);
                $request->session()->put('name', $users[0]->name);
                $request->session()->put('username', $users[0]->username);
                $request->session()->put('role', $role[0]->role);


                if (strtolower($role[0]->role) == 'admin' || strtolower($role[0]->role) == 'petugas') {
                    return redirect('/dashboard/spmp');
                }

                return redirect('/spmp/pengajuan/' . $request->username);
            } else {
                return redirect('/')->with('error', 'Username Tidak Terdaftar');
            }
        } else {

            $siswas = DB::table('siswas')->where('nis', $request->username)->get();

            if (isset($siswas[0]->nis)) {
                if ($request->password != $siswas[0]->nis) {
                    return redirect('/')->with('error', 'Username Tidak Terdaftar');
                } else {
                    $check = $this->checkRegistered($siswas[0]->nis);

                    if (!$check) {
                        $data = ['username' => $siswas[0]->nis, 'password' => Crypt::encryptString($siswas[0]->nis), 'id_role' => 3, 'name' => $siswas[0]->nmlengkap, 'tanggallahir' => $siswas[0]->tanggallahir];
                        $id_user = DB::table('users')->insertGetId($data);

                        $role = DB::table('roles')
                            ->where('id', '=', 3)
                            ->get();

                        $request->session()->put('id', $id_user);
                        $request->session()->put('name', $siswas[0]->nmlengkap);
                        $request->session()->put('username', $siswas[0]->nis);
                        $request->session()->put('role', $role[0]->role);
                    } else {
                        $role = DB::table('roles')
                            ->where('id', '=', $check[0]->id_role)
                            ->get();

                        $request->session()->put('id', $check[0]->id);
                        $request->session()->put('name', $check[0]->name);
                        $request->session()->put('username', $check[0]->username);
                        $request->session()->put('role', $role[0]->role);
                    }

                    if (strtolower($role[0]->role) == 'admin' || strtolower($role[0]->role) == 'petugas') {
                        return redirect('/dashboard/spmp');
                    }

                    return redirect('/spmp/pengajuan/' . $request->username);
                }
            } else {
                return redirect('/')->with('error', 'Username Tidak Terdaftar');
            }
        }
    }

    public function checkRegistered($nis)
    {
        $check = DB::table('users')->where('username', $nis)->get();

        if (isset($check[0]->username)) {
            return $check;
        } else {
            return false;
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }
}
