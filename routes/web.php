<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\Dashboard_SPMPController;
use App\Http\Controllers\Dashboard_SispelingController;
use App\Http\Controllers\Manajemen_SPMP;
use App\Http\Controllers\MyProfileController;
use App\Http\Controllers\PengajuanSPMPController;
use App\Http\Controllers\ThnpelajaranController;
use App\Http\Controllers\TransaksiSPMPController;
use App\Http\Controllers\SispelingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


route::get('/', [AuthController::class, 'login']);

// my profile
route::get('/myprofile', [MyProfileController::class, 'index']);
route::get('/myprofile/history_spmp', [MyProfileController::class, 'historySpmp']);


// manajemen admin
route::resource('/manajemen_admin/role', RoleController::class);
route::resource('/manajemen_admin/user', UserController::class);
route::resource('/manajemen_admin/thnpelajaran', ThnpelajaranController::class);
route::resource('/manajemen_admin/kelas', KelasController::class);
route::resource('/spmp/transaksi', TransaksiSPMPController::class);
route::resource('/spmp/pengajuan', PengajuanSPMPController::class);

// route::get('/spmp/{page}/jurusan', [JurusanController::class, 'index']);
// route::get('/spmp/{page}/jurusan/thnpelajaran/{id_jurusan}', [ThnpelajaranController::class, 'index']);
// route::get('/spmp/{page}/jurusan/thnpelajaran/kelas/{id_jurusan}/{id_thnpelajaran}', [KelasController::class, 'index']);
// route::get('/spmp/{page}/jurusan/thnpelajaran/kelas/siswa/{id_jurusan}/{id_thnpelajaran}/{id_kelas}', [SiswaController::class, 'index']);
// route::post('/spmp/pengajuan/filter', [PengajuanSPMPController::class, 'filter']);

// SPMP =================================================
route::get('/spmp/manajemen', [Manajemen_SPMP::class, 'index']);
route::get('/spmp/pangajuan/approval', [PengajuanSPMPController::class, 'approval']);
route::get('/spmp/{page}/siswa', [SiswaController::class, 'index']);
route::post('/pengajuan/storeApproval', [PengajuanSPMPController::class, 'storeApproval']);
route::post('/spmp/manajemen/cetak_pdf', [Manajemen_SPMP::class, 'cetakPdf']);
route::post('/spmp/pengajuan/cetak_all_pdf', [TransaksiSPMPController::class, 'cetakAllPdf']);
route::post('/spmp/pengajuan/cetak_detail_pdf', [TransaksiSPMPController::class, 'cetakDetailPdf']);
route::post('/spmp/transaksi/delete_detail', [TransaksiSPMPController::class, 'deleteDetail']);
// =======================================================

// dashboard
route::get('/dashboard/spmp', [Dashboard_SPMPController::class, 'index']);
route::get('/dashboard/sispeling', [Dashboard_SispelingController::class, 'index']);
// =======================================================

route::post('/auth/checklogin', [AuthController::class, 'checklogin']);
route::get('/auth/logout', [AuthController::class, 'logout']);

// ajax pengajuan SPMP
route::post('/pengajuan/getTahunAjaran', [PengajuanSPMPController::class, 'getTahunAjaran']);
route::post('/pengajuan/getKelas', [PengajuanSPMPController::class, 'getKelas']);
route::post('/pengajuan/getDataTable', [PengajuanSPMPController::class, 'getDataTable']);

// ajax transaksi
route::post('/spmp/pengajuan/getAll', [PengajuanSPMPController::class, 'getAll']);
route::post('/spmp/transaksi/getAll', [TransaksiSPMPController::class, 'getAll']);
route::post('/spmp/detail_history/getAll', [MyProfileController::class, 'getAll']);
route::post('/spmp/transaksi/editNominal', [TransaksiSPMPController::class, 'editNominal']);

// Sispeling
route::post('/sispeling/cetak_pdf', [SispelingController::class, 'cetakPdf']);
route::get('/sispeling', [SispelingController::class, 'index']);
route::get('/sispeling/pembayaran', [SispelingController::class, 'pembayaran']);
route::get('/sispeling/pembayaran/edit/{id}', [SispelingController::class, 'edit']);
route::post('/sispeling/store', [SispelingController::class, 'store']);

// ajax sispeling
route::post('/sispeling/getKelas', [SispelingController::class, 'getKelas']);
route::post('/sispeling/pembayaran/getAngkatan', [SispelingController::class, 'getAngkatan']);

// dashboard ==
route::post('/sispeling/dashboard/getTotal', [Dashboard_SispelingController::class, 'getTotal'])->name('sispeling.dashboard.getTotal');
route::post('/sispeling/dashboard/columnChart', [Dashboard_SispelingController::class, 'columnChart'])->name('sispeling.dashboard.columnChart');
route::post('/spmp/dashboard', [Dashboard_SPMPController::class, 'dashboard'])->name('spmp.dashboard');

route::post('/changepassword', [MyProfileController::class, 'changePassword']);
route::post('/changephoto', [MyProfileController::class, 'changePhoto']);

// datatable ===
route::get('/users/datatable', [UserController::class, 'datatable'])->name('users.datatable');
route::get('/roles/datatable', [RoleController::class, 'datatable'])->name('roles.datatable');
route::get('/thnpelajaran/datatable', [ThnpelajaranController::class, 'datatable'])->name('thnpelajaran.datatable');
route::get('/kelas/datatable', [KelasController::class, 'datatable'])->name('kelas.datatable');
route::get('/sispeling/datatable', [SispelingController::class, 'DTSispeling'])->name('sispeling.datatable');
route::get('/spmp/approval/datatable', [PengajuanSPMPController::class, 'DTSPMPApproval'])->name('spmp.approval.datatable');
route::get('/spmp/manajemen/datatable', [Manajemen_SPMP::class, 'DTManajemenSPMP'])->name('spmp.manajemen.datatable');
// datatable ===